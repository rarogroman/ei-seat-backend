const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const fileUpload = require('express-fileupload');

// Init App
const app = express();

app.use(cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.use(fileUpload());



// Init CLIENT-API routes
const clientApiStadiumsRoute = require('./client_api/routes/stadiums');
const clientApiAuthRoute = require('./client_api/routes/auth');
const clientApiProfilesRoute = require('./client_api/routes/profiles');
const clientApiOrdersRoute = require('./client_api/routes/orders');
const clientApiNotificationsRoute = require('./client_api/routes/notifications');
const clientApiEmailRoute = require('./client_api/routes/email');

// Init VENDOR-API routes
const vendorApiAuthRoute = require('./vendor_api/routes/auth');
const vendorApiManagement = require('./vendor_api/routes/management');
const vendorApiOrders = require('./vendor_api/routes/orders');
const vendorApiPush = require('./vendor_api/routes/push');

// Init ADMIN-API routes
const adminAuthRoute = require('./admin_api/routes/auth');
const stadiumRoute = require('./admin_api/routes/stadiums');
const standRoute = require('./admin_api/routes/stands');
const kioskRoute = require('./admin_api/routes/kiosks');
const productRoute = require('./admin_api/routes/products');
const categoryRoute = require('./admin_api/routes/categories');
const orderAuthRoute = require('./admin_api/routes/orders');
const vendorAuthRoute = require('./admin_api/routes/vendors');
const andminApiNewsRoute = require('./admin_api/routes/news');

// Custom Middlewares
const corsHandler = require('./app/middlewares/cors-handler'),
notFoundHandler = require('./app/middlewares/not-found-handler');
errorHandler = require('./app/middlewares/error-handler');

// Logging
morgan.format('myformat', '[:date[web]] [:remote-addr]":method :url" :status :res[content-length] - :response-time ms');
app.use(morgan('myformat'));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());                                     // Middleware for bodyparser
app.use(corsHandler);                                           // Middleware for CORS request

// ADMIN-API routes
app.use('/admin-api/auth', adminAuthRoute);
app.use('/admin-api/stadiums', stadiumRoute);
app.use('/admin-api/stands', standRoute);
app.use('/admin-api/kiosks', kioskRoute);
app.use('/admin-api/products', productRoute);
app.use('/admin-api/categories',categoryRoute);
app.use('/admin-api/orders', orderAuthRoute);
app.use('/admin-api/vendors', vendorAuthRoute);
app.use('/admin-api/news', andminApiNewsRoute);

// CLIENT-API routes
app.use('/client-api/stadiums', clientApiStadiumsRoute);
app.use('/client-api/auth', clientApiAuthRoute);
app.use('/client-api/profile', clientApiProfilesRoute);
app.use('/client-api/orders', clientApiOrdersRoute);
app.use('/client-api/notifications', clientApiNotificationsRoute);
app.use('/client-api/email', clientApiEmailRoute);

// VENDOR-API routes
app.use('/vendor-api/auth', vendorApiAuthRoute);
app.use('/vendor-api/management', vendorApiManagement);
app.use('/vendor-api/orders', vendorApiOrders);
app.use('/vendor-api/push', vendorApiPush);

// for using the static pictures on heroku
app.use(express.static('/var/www/seat/media/images/'));

// Middlware
app.use(notFoundHandler);                                       // not found page
app.use(errorHandler);                                          // error processing

module.exports = app;
