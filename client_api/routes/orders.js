const express = require('express');
const router = express.Router();
const OrdersController = require('../controllers/orders');
const asyncHandler = require('express-async-handler');
const verifyClientToken = require('../../app/middlewares/verifyClientToken');


router.post('/create', verifyClientToken,
    asyncHandler(OrdersController.createOrder())
);

router.post('/history', verifyClientToken,
    asyncHandler(OrdersController.getHistoryOrders())
);

router.post('/detailed', verifyClientToken,
    asyncHandler(OrdersController.getOderDetailed())
);

module.exports = router;