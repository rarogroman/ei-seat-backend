const express = require('express');
const router = express.Router();
const СlientAuthController = require('../controllers/auth');
const asyncHandler = require('express-async-handler');
const verifyClientToken = require('../../app/middlewares/verifyClientToken');


router.post('/register',
    asyncHandler(СlientAuthController.clientRegister())
);

router.post('/login',
    asyncHandler(СlientAuthController.clientLogin())
);

router.post('/forgot-password',
    asyncHandler(СlientAuthController.forgotPassword())
);

router.post('/change-password', verifyClientToken,
    asyncHandler(СlientAuthController.changePassword())
);

router.post('/facebook-login',
    asyncHandler(СlientAuthController.facebookLogin())
);

router.get('/page-reset-password',
    asyncHandler(СlientAuthController.pageResetPassword())
);


module.exports = router;