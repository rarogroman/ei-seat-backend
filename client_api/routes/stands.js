const express = require('express');
const router = express.Router();
const StandsController = require('../controllers/stands');
const asyncHandler = require('express-async-handler');


router.post('/categories',
    asyncHandler(StandsController.getCategoriesList())
);

router.post('/products',
    asyncHandler(StandsController.getProductListByStandAndCategory())
);


module.exports = router;