const express = require('express');
const router = express.Router();
const ProfilesController = require('../controllers/profiles');
const asyncHandler = require('express-async-handler');
const verifyClientToken = require('../../app/middlewares/verifyClientToken');


router.post('/me', verifyClientToken,
    asyncHandler(ProfilesController.me())
);

router.post('/update', verifyClientToken,
    asyncHandler(ProfilesController.updateProfileInfo())
);


module.exports = router;