const express = require('express');
const router = express.Router();
const ClientEmailController = require('../controllers/email');
const asyncHandler = require('express-async-handler');


router.post('/send',
    asyncHandler(ClientEmailController.sendEmail())
);


module.exports = router;
