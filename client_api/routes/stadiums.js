const express = require('express');
const router = express.Router();
const StadiumsController = require('../controllers/stadiums');
const StandsController = require('../controllers/stands');
const asyncHandler = require('express-async-handler');


router.post('/',
    asyncHandler(StadiumsController.getStadiumsList())
);

router.post('/stands',
    asyncHandler(StadiumsController.getStandsList())
);

router.post('/stands/categories',
    asyncHandler(StandsController.getCategoriesList())
);

router.post('/stands/products',
    asyncHandler(StandsController.getProductListByStandAndCategory())
);

module.exports = router;