const express = require('express');
const router = express.Router();
const NotificationsController = require('../controllers/notifications');
const asyncHandler = require('express-async-handler');
const verifyClientToken = require('../../app/middlewares/verifyClientToken');


router.post('/all', verifyClientToken,
    asyncHandler(NotificationsController.getAllNotifications())
);

router.post('/mark-readed-all', verifyClientToken,
    asyncHandler(NotificationsController.markAllNotificationsReaded())
);

router.post('/unread-count', verifyClientToken,
    asyncHandler(NotificationsController.getUnreadNotificationsCount())
);

module.exports = router;