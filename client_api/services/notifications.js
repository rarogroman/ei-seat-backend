const mysql = require('../../app/drivers/mysql');
const settings = require('../../settings');


class NotificationsService {

    /* Get all notifications for user_id */
    static getAllNotifications(userId) {

        return async () => { 

            let sql  = `SELECT 
                            order_notifications.order_id, 
                            order_notifications.order_notification_status AS status, 
                            order_notifications.order_notification_date as date, 
                            order_notifications.order_notification_read AS already_read,
                            orders.order_readiness_time,
                            orders.order_kiosk_id,
                            orders.order_delivery_section,
                            orders.order_delivery_row,
                            orders.order_delivery_seat
                        FROM 
                            orders 
                        INNER JOIN 
                            order_notifications ON orders.order_id = order_notifications.order_id
                        WHERE 
                            orders.user_id='${userId}' AND
                            order_notifications.order_notification_status IN (  'DECLINED',
                                                                                'PREPARING',
                                                                                'READY_FOR_COLLECTION',
                                                                                'COLLECTED',
                                                                                'ON_ITS_WAY',
                                                                                'DELIVERED')
                        ORDER BY
                            order_notifications.order_notification_date DESC;`;

            const listNotifications = await mysql().query(sql);

            for (const item of listNotifications) {
                item.already_read = item.already_read? true:false;
                item.message = settings.ORDER_NOTIFICATIONS[item.status]
                    .replace('ORDNUMBER', item.order_id)
                    .replace('READINESSTIME', item.order_readiness_time)
                    .replace('KIOSKNUMBER', item.order_kiosk_id)
                    .replace('SECTION', item.order_delivery_section)
                    .replace('ROWNUMBER', item.order_delivery_row)
                    .replace('SEATNUMBER', item.order_delivery_seat);
            }
            return listNotifications;
        }
    }

    /* Mark all notification for user as was readed */
    static markAllNotificationsReaded(userId){

        return async () => { 

            let sql  = `UPDATE 
                            order_notifications
                        SET
                            order_notifications.order_notification_read = '1'
                        WHERE
                            order_notifications.order_id IN 
                                (
                                    SELECT 
                                        orders.order_id 
                                    FROM 
                                        orders 
                                    WHERE 
                                        orders.user_id='${userId}'
                                ) `;

            const updateResult = await mysql().query(sql);

            if (updateResult) {
                return {
                    'success': true,
                    'message': 'Information has been updated.'
                    };
            } else {
                return {
                    'success': false,
                    'message': 'Information has not been updated.'
                    };
            }
        }
    }

    /* Get count unreaded notifications for user_id */
    static getUnreadNotificationsCount(userId){

        return async () => { 
            let sql  = `
                    SELECT SUM(unread_count) as unread_count FROM (
                        SELECT 
                            orders.user_id, 
                            Count(order_notifications.order_notification_read) AS unread_count
                        FROM 
                            orders 
                        INNER JOIN 
                            order_notifications ON orders.order_id = order_notifications.order_id
                        GROUP BY 
                            orders.user_id, order_notifications.order_notification_read,order_notifications.order_notification_status
                        HAVING 
                            order_notifications.order_notification_status IN (${JSON.stringify(Object.keys(settings.ORDER_NOTIFICATIONS)).slice(1, -1)}) AND
                            orders.user_id='${userId}' AND 
                            order_notifications.order_notification_read='0' 
                    ) as query`;

            const unreadCount = (await mysql().query(sql))[0];

            if (unreadCount) {
                return {
                    'success': true,
                    'user_id': userId,
                    'unread_count': unreadCount.unread_count
                    }
            } else {
                return {
                    'success': true,
                    'user_id': userId,
                    'unread_count': 0
                }
            }
        }
    }


}

module.exports = NotificationsService;