const mysql = require('../../app/drivers/mysql');
const JWT = require('jsonwebtoken');
const settings = require('../../settings');
const bcrypt = require('bcryptjs');


class ProfilesService {

    /* Update client profile info */
    static updateProfileInfo(accessToken,
                            bodyUserName,
                            bodyEmail,
                            bodyFirstName,
                            bodyLastName,
                            bodyDateOfBirth,
                            bodyPhoneNumber,
                            bodyPassword) {

        return async () => {

            if (! accessToken) {
                console.log('Access token is empty');
                return {
                    'success': false,
                    'message': 'Access token is empty.'
                }
            }

            const decoded = JWT.decode(accessToken, settings.JWT.clientAccessTokenSecret);
            const userId = decoded.sub;

            // userName, email, password - required fields
            if (! bodyUserName) {
                return {
                    'success': false,
                    'message': 'User name is empty.'
                    };
            }


            if (! bodyEmail) {
                return {
                    'success': false,
                    'message': 'Email is empty.'
                    };
            }

/*
            if (! bodyPassword) {
                return {
                    'success': false,
                    'message': 'Password is empty.'
                    };
            }
*/

            // check that the user with such userId exists
            let sql  = `SELECT
                            *
                        FROM
                            users
                        WHERE
                            users.user_id = '${userId}'
                        LIMIT 1`;

            const checkUser = (await mysql().query(sql))[0];

            if (! checkUser) {
                return {
                    'success': false,
                    'message': 'Access denied, authentication failed.'
                    };
            }

            // change email ? Check that such email is not registered another user
            if (checkUser.user_email !== bodyEmail) {

                sql  = `SELECT
                            *
                        FROM
                            users 
                        WHERE
                            users.user_email = '${bodyEmail}'
                        LIMIT 1`;

                const checkEmailExist = (await mysql().query(sql))[0];
                if ( checkEmailExist) {
                return {
                    'success': false,
                    'message': 'User with such email already registered.'
                    };
                }
            }

            // verify success, then update info about user
            if (bodyPassword) {

	        const hashedPassword = bcrypt.hashSync(bodyPassword, 10);
            	sql  = `UPDATE
                	        users
                    	SET
		  		 users.user_name = '${bodyUserName}',
	                        users.user_email = '${bodyEmail}',
                	        users.user_first_name = ${ (bodyFirstName)? "'"+bodyFirstName+"'":'NULL'},
	                        users.user_last_name = ${ (bodyLastName)? "'"+bodyLastName+"'":'NULL'},
	                        users.user_date_of_birth = ${ (bodyDateOfBirth)? "'" + bodyDateOfBirth + "'" : 'NULL'},
	                        users.user_phone_number = ${ (bodyPhoneNumber)? "'"+bodyPhoneNumber+"'":'NULL'},
	                        users.user_password = '${hashedPassword}'
	                    WHERE
	                        users.user_id = '${userId}'
	                    LIMIT 1`;
             } else {

                  sql  = `UPDATE
                                users
                        SET
                                 users.user_name = '${bodyUserName}',
                                users.user_email = '${bodyEmail}',
                                users.user_first_name = ${ (bodyFirstName)? "'"+bodyFirstName+"'":'NULL'},
                                users.user_last_name = ${ (bodyLastName)? "'"+bodyLastName+"'":'NULL'},
                                users.user_date_of_birth = ${ (bodyDateOfBirth)? "'" + bodyDateOfBirth + "'" : 'NULL'},
                                users.user_phone_number = ${ (bodyPhoneNumber)? "'"+bodyPhoneNumber+"'":'NULL'}
                            WHERE
                                users.user_id = '${userId}'
                            LIMIT 1`;

}
console.log('************************************');
console.log(sql);
            const updateResult = await mysql().query(sql);
console.log('updateResult: ', updateResult);
            // Information about the user has been updated ?
            if (updateResult.affectedRows) {
                return {
                    'success': true,
                    'message': 'Your profile has been updated.'
                    };
            } else {
                return {
                    'success': false,
                    'message': 'Your profile has not been updated.'
                    };
            }
        }
    }

    /* Find user by user_id  */
    static findUser(userId) {

        return async () => { 
                
            let sql  = `SELECT
                            users.user_id AS id,
                            users.user_name AS name,
                            users.user_first_name AS first_name,
                            users.user_last_name AS last_name,
                            users.user_date_of_birth AS date_of_birth,
                            users.user_phone_number AS phone_number,
                            users.user_email AS email
                        FROM
                            users 
                        WHERE
                            users.user_id = '${userId}'
                        LIMIT 1`;

            return mysql().query(sql);
        }
    }

}

    

module.exports = ProfilesService;
