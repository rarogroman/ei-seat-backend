const mysql = require('../../app/drivers/mysql');
const JWT = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const settings = require('../../settings');
const request = require('request-promise');
const MailUtils = require('../../app/helpers/mailUtils');


class ClientAuthService {

    /* Register new client in table users */
    static clientRegister(clientEmail, clientPassword, clientName, clientPhoneNumber) {

        return async () => {

            // user with such an email already exists in the database ?
            let sql  = `SELECT
                            users.user_email AS email,
                            users.user_password AS password
                        FROM
                            users
                        WHERE
                            users.user_email = '${clientEmail}'
                        LIMIT 1`;

            let dbUser = (await mysql().query(sql))[0];

            if (dbUser) {
                return {
                    'success': false,
                    'message': 'This email is already registered.'
                };
            } else {
                const hashedPassword = bcrypt.hashSync(clientPassword, 10);

                let sql  = `INSERT INTO users (
                                        user_name,
                                        user_email,
                                        user_password,
                                        user_phone_number )
                            VALUES (
                                        '${clientName}',
                                        '${clientEmail}',
                                        '${hashedPassword}',
                                         ${(clientPhoneNumber)? "'"+clientPhoneNumber+"'":'NULL'}  )`

                const successNewUser = await mysql().query(sql);

                if (successNewUser) {

                    let sql  = `SELECT
                                    users.user_id AS id,
                                    users.user_name AS name,
                                    users.user_phone_number AS phone_number,
                                    users.user_email AS email
                                FROM
                                    users
                                WHERE
                                    users.user_email = '${clientEmail}' AND
                                    user_password = '${hashedPassword}'
                                LIMIT 1`;

                    const dbUser = (await mysql().query(sql))[0];

                    // get accessToken
                    const accessToken = JWT.sign({
                        iss: "Seat",
                        sub: dbUser.id,
                        email: dbUser.email,
                        // exp: new Date().setDate(new Date().getDate() )/1000 + settings.tokenLife
                        }, settings.JWT.clientAccessTokenSecret);

                    return {
                        'success': true,
                        'message': 'New user successfully registered.',
                        'accessToken': accessToken,
                        'id': dbUser.id,
                        'name': dbUser.name,
                        'phone_number': dbUser.phone_number,
                        'email': dbUser.email
                    }
                } else {
                    return{
                        'success': false,
                        'message': 'Registration failed.'
                    };
                }
            }
        }
    }

    /* Login client by email and password*/
    static clientLogin(clientEmail, clientPassword) {

        return async () => {

            let sql  = `SELECT
                            users.user_id AS id,
                            users.user_email AS email,
                            users.user_password AS password,
                            users.user_name AS name,
                            users.user_date_of_birth AS date_of_birth,
                            users.user_phone_number AS phone_number
                        FROM
                            users
                        WHERE
                            users.user_email = '${clientEmail}'
                        LIMIT 1`;

            const dbUser = (await mysql().query(sql))[0];

            if (! dbUser) {
                // this email not found in database
                return {
                    'success': false,
                    'message': 'This email is not valid.'
                    };
            } else {
                // email was found in dataBase -> check the password
                const resultCompare = bcrypt.compareSync(clientPassword, dbUser.password)

                if (resultCompare) {
                    const accessToken = JWT.sign({
                        iss: "Seat",
                        sub: dbUser.id,
                        email: dbUser.email,
                        // exp: new Date().setDate(new Date().getDate() )/1000 + settings.tokenLife
                        }, settings.JWT.clientAccessTokenSecret);

                        return {
                            'success': true,
                            'accessToken': accessToken,
                            'id': dbUser.id,
                            'name': dbUser.name,
                            'date_of_birth': dbUser.date_of_birth,
                            'phone_number': dbUser.phone_number? dbUser.phone_number:"",
                            'email': dbUser.email
                            };
                    } else {

                        return {
                            'success': false,
                            'message': 'Password is not valid.'
                        };
                    }
            }
        }
    }

    /* If email exist in database, then send to mail the password recovery link */
    static forgotPassword(clientEmail) {

        return async () => {

            let sql  = `SELECT * FROM users WHERE users.user_email = '${clientEmail}' LIMIT 1`;
            const findUser = (await mysql().query(sql))[0];
            if (! findUser) {
                return {
                    'success': false,
                    'message': 'User with such a email does not exist.'
                };
            }

            // create the short-lived token for reset password
            const token = JWT.sign({
                iss: "Seat",
                sub: findUser.user_id,
                email: findUser.user_email,
                exp: new Date().setDate(new Date().getDate() )/1000 + settings.JWT.resetPasswordTokenLife
                }, settings.JWT.clientAccessTokenSecret );

            const linkResetPassword = `${settings.linkResetPassword}?token=${token}`;

            const html = `You recently requested to reset your password for your SEAT FDC account.<br>
              Click the link below to reset it.<br><br>
              <a href="${linkResetPassword}"> Click to reset your password </a> <br><br>
              If you did not request a password reset, please ignore this email or reply to let us know.<br>
              This password reset is only valid for the next 10 minutes.<br><br>
              Thanks,<br>
              SEAT FDC`;

            const resultSendMail = await MailUtils.sendMail(clientEmail, html, 'Reset your password for SEAT FDC account')();

            return resultSendMail;
        }
    }

    /* Change password for client if client have a short-lived token*/
    static changePassword(shortLivedToken, newPassword) {

        return async () => {

            const decoded = JWT.decode(shortLivedToken, settings.JWT.clientAccessTokenSecret);
            const userId = decoded.sub;
            const hashedPassword = bcrypt.hashSync(newPassword, 10);

            let sql  = `UPDATE
                            users
                        SET
                            users.user_password = '${hashedPassword}'
                        WHERE
                            users.user_id = '${userId}'
                        LIMIT 1`;

            const updateResult = await mysql().query(sql);

            if (updateResult.affectedRows) {

                let sql  = `SELECT
                                users.user_id AS id,
                                users.user_email AS email,
                                users.user_password AS password,
                                users.user_name AS name,
                                users.user_date_of_birth AS date_of_birth,
                                users.user_phone_number AS phone_number
                            FROM
                                users
                            WHERE
                                users.user_id = '${userId}'
                            LIMIT 1`;

                const dbUser = (await mysql().query(sql))[0];

                const accessToken = JWT.sign({
                    iss: "Seat",
                    sub: dbUser.id,
                    email: dbUser.email,
                    // exp: new Date().setDate(new Date().getDate() )/1000 + settings.tokenLife
                    }, settings.JWT.clientAccessTokenSecret );

                return {
                    'success': true,
                    'message': 'Your password has been updated.',
                    'accessToken': accessToken,
                    'id': dbUser.id,
                    'name': dbUser.name,
                    'date_of_birth': dbUser.date_of_birth,
                    'phone_number': dbUser.phone_number,
                    'email': dbUser.email

                };
            } else {
                return {
                    'success': false,
                    'message': 'Your password has not been updated.'
                };
            }
        }
    }

    /* Get facebook long-lived token for short-lived token*/
    static getFacebookLongLivedToken(facebookShortLivedToken) {

        let requestUri = "https://graph.facebook.com/oauth/access_token?";
            requestUri += "grant_type=fb_exchange_token" + "&";
            requestUri += "client_id=" + settings.oauth.facebook.clientId + "&";
            requestUri += "client_secret=" + settings.oauth.facebook.secretKey + "&";
            requestUri += "fb_exchange_token=" + facebookShortLivedToken;


            let options = {
                uri: requestUri,
                method: "GET",
                json: true
            }
            return request(options);
    }

    /* Get info about user for facebook long-lived token*/
    static getFacebookUser(facebookLongLivedToken) {

        let fields = ['first_name', 'last_name', 'email'];

        let requestUri = "https://graph.facebook.com/v3.0/me?access_token=" + facebookLongLivedToken + "&";
            requestUri += "fields=" + fields.join(',');

        let options = {
            uri: requestUri,
            method: "GET",
            json: true
        }

        return request(options);
    }

    /* Get info about client from database table users */
    static getUser(facebookUser) {

        let sql = `SELECT
                        *
                    FROM
                        users
                    WHERE
                        (users.user_fb_account_id = '${facebookUser.id}') OR (users.user_email = '${facebookUser.email}')
                    LIMIT 1`;
        return mysql().query(sql);
    }

    /* Create new user with facebook user info, if user not registered in database*/
    static createUser(facebookUser) {

        let sql =  `INSERT INTO
                        users (
                            users.user_name,
                            users.user_email,
                            user_fb_account_id )
                        VALUES (
                            "${facebookUser.first_name} ${facebookUser.last_name}",
                             ${ (facebookUser.email)? "'"+facebookUser.email+"'":'NULL'},
                             ${facebookUser.id}    )`;
        return mysql().query(sql);
    }


}

module.exports = ClientAuthService;
