const mysql = require('../../app/drivers/mysql');


class StadiumsService {

    /* Get full list of stadiums */
    static getStadiumsList() {
        
        let fieldsList = [
            "stadiums.stadium_id AS id",                
            "stadiums.stadium_name AS name",           
            "stadiums.stadium_featured_url AS image"  
        ];

        let sql = "SELECT " + fieldsList.join(', ') + " FROM stadiums";
        
        return mysql().query(sql);
    }

    /* Get list all stands for one stadium by stadium_id */
    static getStandsList(stadiumId) {

        let sql = `SELECT
                        stands.stand_id AS id,
                        stands.stand_name AS name
                    FROM 
                        stands
                    WHERE 
                        stands.stadium_id = ${stadiumId}`;

        return mysql().query(sql);
    }
   
}

module.exports = StadiumsService;