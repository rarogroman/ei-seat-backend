const mysql = require('../../app/drivers/mysql');


class StandsService {

    /* Get list all categories of products for one stand by stand_id */
    static getCategoriesListByStandId(standId) {
        
        let sql =  `SELECT 
                        categories.category_id AS id, 
                        categories.category_name AS name
                    FROM stands
                    INNER JOIN kiosks ON stands.stand_id = kiosks.stand_id
                    INNER JOIN warehouses ON kiosks.kiosk_id = warehouses.kiosk_id
                    INNER JOIN products ON products.product_id = warehouses.product_id
                    INNER JOIN categories ON categories.category_id = products.category_id
                    WHERE 
                        stands.stand_id = '${parseInt(standId)}' AND
                        warehouses.warehouse_enable = '1' AND
                        kiosks.kiosk_enable = '1'
                    GROUP BY categories.category_id
                    ORDER BY categories.category_name ASC`

        return mysql().query(sql);

    }

    /* Get list all products for one stand and for one category */      
    static getProductListByStandAndCategory(standId, categoryId) {

        let sql  = `SELECT 
                        products.product_id AS id, 
                        products.product_name AS name, 
                        products.product_price AS price
                    FROM stands
                    INNER JOIN kiosks ON stands.stand_id = kiosks.stand_id
                    INNER JOIN warehouses ON kiosks.kiosk_id = warehouses.kiosk_id
                    INNER JOIN products ON products.product_id = warehouses.product_id
                    WHERE 
                        stands.stand_id = '${parseInt(standId)}' AND
                        warehouses.warehouse_enable = '1' AND
                        kiosks.kiosk_enable = '1' AND
                        products.category_id ='${parseInt(categoryId)}'
                    GROUP BY products.product_id
                    ORDER BY products.product_name ASC`

        return mysql().query(sql);
    }

}

module.exports = StandsService;