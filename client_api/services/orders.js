const mysql = require('../../app/drivers/mysql');


class OrdersService {

    /*  Create new order in table orders, add products in table order_items ==*/
    static createOrder( stadiumId,
                        stanId,
                        userId,
                        stripeToken,
                        deviceOs,
                        deviceToken,
                        delivery,
                        deliverySection,
                        deliveryRow,
                        deliverySeat,
                        products) {

        return async () => {
console.log('service create order');
            // filter list of products when quantity > 0
            products = products.filter(function(item, idx) {
                return products[idx].product_quantity > 0;
            });

            // if basket is empty then return success: false
            if (products.length <= 0) {
                return {
                    'success': false,
                    'message': 'Your basket is empty.'
                    };
            };

            // get kiosk_id where mimimum numbers the orders for today
            let sql  = `SELECT * FROM (
                            SELECT
                                kiosks.kiosk_id
                            FROM
                                kiosks
                            WHERE
                                kiosks.kiosk_enable = '1' AND
                                kiosks.stand_id = '${stanId}'
                            ) AS A
                        LEFT JOIN (
                        SELECT
                            orders.order_kiosk_id,
                            Count(orders.order_kiosk_id) AS count
                        FROM
                            orders
                        WHERE
                                orders.order_stand_id = '${stanId}' AND 
                                DATE(orders.order_date_created) = CURDATE()
                        GROUP BY
                            orders.order_kiosk_id
                        ) AS B
                        ON A.kiosk_id=B.order_kiosk_id
                        ORDER BY B.count
                        LIMIT 1 `;

            const result = (await mysql().query(sql))[0];

            let kioskWithMinCountOrders = 0;
            let defaultKioskNumber = undefined;
            if (result) {
                kioskWithMinCountOrders = result.kiosk_id;
            } else {
                // kiosk number first, then status will DECLINED
                let sql  = `SELECT
                                kiosks.kiosk_id
                            FROM
                                kiosks
                            LIMIT 1 `;

                const resultKioskId = (await mysql().query(sql))[0];
                defaultKioskNumber = resultKioskId.kiosk_id;
            }

            // insert new order in table orders
            const dateOrder = new Date().toISOString().slice(0, 19).replace('T', ' ');
            const deliveryTinyInt = delivery ? 1 : 0;

            sql  = `INSERT INTO orders (
                        user_id,
                        order_stadium_id,
                        order_stand_id,
                        order_kiosk_id,
                        order_stripe_token,
                        order_device_os,
                        order_device_token,
                        order_delivery,
                        order_delivery_section,
                        order_delivery_row,
                        order_delivery_seat,
                        order_date_created,
                        order_status
                        )
                    VALUES (
                        '${userId}',
                        '${stadiumId}',
                        '${stanId}',
                        '${kioskWithMinCountOrders || defaultKioskNumber}',
                        '${stripeToken}',
                        '${deviceOs}',
                         ${(deviceToken)? "'"+deviceToken+"'":'NULL'},
                        '${deliveryTinyInt}',
                         ${(deliverySection)? "'"+deliverySection+"'":'NULL'},
                         ${(deliveryRow)? "'"+deliveryRow+"'":'NULL'},
                         ${(deliverySeat)? "'"+deliverySeat+"'":'NULL'},
                        '${dateOrder}',
                        'PENDING'
                    )`;

            let updateResult = await mysql().query(sql);

            if (updateResult.affectedRows) {
                // if new order was created in table orders, than get the order_id
                sql  = `SELECT
                            order_id
                        FROM
                            orders
                        WHERE
                            user_id = '${userId}' AND
                            order_date_created = '${dateOrder}'
                        LIMIT 1`;

                const orderId = (await mysql().query(sql))[0].order_id;

                // Create sql for insert products in table order_items
                let sqlInsertOrderItems = 'INSERT INTO order_items (order_id, product_id, order_item_quantity, order_item_price) VALUES';
                for (const item of Object.keys(products)) {
                    sqlInsertOrderItems += ` ('${orderId}', '${products[item].product_id}', '${products[item].product_quantity}', (SELECT products.product_price FROM products WHERE products.product_id = '${products[item].product_id}' ) ),`;
                }

                updateResult = (await mysql().query(sqlInsertOrderItems.slice(0,-1)));

                if ((updateResult.affectedRows == products.length) && (kioskWithMinCountOrders != 0)) {
                    // order items was added success to database
                    // insert into table `order_statuser` with statese PENDING
                    sql = `INSERT INTO order_notifications (
                                order_notifications.order_id,
                                order_notifications.order_notification_status,
                                order_notifications.order_notification_date )
                            VALUES (
                                '${orderId}',
                                'PENDING',
                                 NOW() )`;

                    updateResult = await mysql().query(sql);

                    return {
                        'success': true,
                        'order_id': orderId,
                        'message': 'Your order was successfully created.'
                        };
                } else if (kioskWithMinCountOrders == 0) {
                    // all kiosks are closed
                    sql  = `UPDATE 
                                orders
                            SET
                                orders.order_status = 'DECLINED'
                            WHERE
                                orders.order_id = '${orderId}' `;

                    updateResult = await mysql().query(sql);

                    // insert into table `order_statuser` with statese PENDING
                    sql = `INSERT INTO order_notifications (
                        order_notifications.order_id,
                        order_notifications.order_notification_status,
                        order_notifications.order_notification_date )
                    VALUES (
                        '${orderId}',
                        'DECLINED',
                         NOW() )`;

                    updateResult = await mysql().query(sql);

                    return {
                        'success': false,
                        'order_id': orderId,
                        'message': 'All kiosks are closed.'
                    };
                } else {
                    // error added order items, order change status to NOT_VALID
                    sql  = `UPDATE 
                                orders
                            SET
                                orders.order_status = 'NOT_VALID'
                            WHERE
                                orders.order_id = '${orderId}' `;

                    updateResult = await mysql().query(sql);

                    return {
                        'success': false,
                        'order_id': orderId,
                        'message': 'Can not added products an order_items.'
                    };
                }

            } else {
                return {
                    'success': false,
                    'message': 'Can not create an order.'
                };
            }
        }
    }

    /* Full list orders with detailed info for client_id */
    static getHistoryOrders(userId) {

        return async () => {

            let sql  = `SELECT
                            orders.order_id AS id,
                            stadiums.stadium_name AS stadium,
                            stands.stand_name AS stand,
                            orders.order_kiosk_id,
                            orders.order_delivery AS delivery,
                            orders.order_delivery_section,
                            orders.order_delivery_row,
                            orders.order_delivery_seat,
                            orders.order_date_created AS created,
                            orders.order_readiness_time,
                            orders.order_status,
                            Sum(order_item_quantity*order_item_price ) AS total_price
                        FROM
                            ((orders
                        INNER JOIN
                            order_items ON orders.order_id = order_items.order_id)
                        INNER JOIN stadiums ON orders.order_stadium_id = stadiums.stadium_id)
                        INNER JOIN stands ON orders.order_stand_id = stands.stand_id
                        GROUP BY
                            orders.order_id,
                            stadiums.stadium_name,
                            stands.stand_name,
                            orders.order_kiosk_id,
                            orders.order_delivery,
                            orders.order_delivery_section,
                            orders.order_delivery_row,
                            orders.order_delivery_seat,
                            orders.order_date_created,
                            orders.order_readiness_time,
                            orders.order_status,
                            orders.user_id
                        HAVING (((orders.user_id)=${userId}));
            `;

            const historyOrders = await mysql().query(sql);

            for (const item in historyOrders) {
                historyOrders[item].delivery = historyOrders[item].delivery? true:false;
            }

            return historyOrders;
        }

    }

    /* Detailed info about order with list of products */
    static getOderDetailed(userId, orderId) {

        return async () => {

            // get order detailed info by order_id
            let sql  = `SELECT
                            orders.user_id,
                            orders.order_id,
                            (SELECT stadium_name FROM stadiums WHERE stadium_id = orders.order_stadium_id LIMIT 1) AS stadium,
                            (SELECT stand_name FROM stands WHERE stand_id = orders.order_stand_id LIMIT 1) AS stand,
                            orders.order_stand_id,
                            orders.order_kiosk_id,
                            orders.order_delivery,
                            orders.order_delivery_section,
                            orders.order_delivery_row,
                            orders.order_delivery_seat,
                            orders.order_date_created,
                            orders.order_readiness_time,
                            orders.order_status,
                            (SELECT SUM(t) from (SELECT order_items.order_item_quantity*order_items.order_item_price AS t FROM order_items WHERE order_items.order_id = '${orderId}') as total) as total_price
                        FROM
                            orders 
                        WHERE
                            orders.order_id = '${orderId}'
                        LIMIT 1`;

            const order = (await mysql().query(sql))[0];

            if (! order) {
                return {
                    'success': false,
                    'message': `Order with number '${orderId}' does not exist.`
                    };
            }

            // Checking that this order belongs to the userId
            if (userId != order.user_id) {
                return {
                    'success': false,
                    'message': `Order with number '${orderId}' does not belong to you.`
                    };
            }

            // get order_items for order_id
            let sqlOrderItems  = `SELECT
                                    (SELECT product_name FROM products WHERE product_id = order_items.product_id LIMIT 1) AS name,
                                    order_items.order_item_quantity AS quantity,
                                    order_items.order_item_price AS price
                                FROM
                                    order_items 
                                WHERE
                                    order_items.order_id = '${orderId}'
                                ORDER BY order_items.order_item_id`;

            const orderItems = await mysql().query(sqlOrderItems);

            return {
                'success': true,
                'id': order.order_id,
                'stadium': order.stadium,
                'stand': order.stand,
                'kiosk_id': order.order_kiosk_id,
                'delivery': order.order_delivery ? true : false,
                'delivery_section': order.order_delivery_section,
                'delivery_row': order.order_delivery_row,
                'delivery_seat': order.order_delivery_seat,
                'created': (new Date(order.order_date_created-(new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, 19),
                'readiness_time': order.order_readiness_time,
                'status': order.order_status,
                'total_price': order.total_price,
                'products': orderItems
                };
        }
    }
    
}

module.exports = OrdersService;
