const NotificationsService = require('../services/notifications');
const JWT = require('jsonwebtoken');
const settings = require('../../settings');

class NotificationsController {

    /* Get all notifications for user_id */
    static getAllNotifications() {

        return async (req, res, next) => {

            const accessToken = req.body.accessToken || req.query.accessToken || req.headers.accesstoken;

            const decoded = JWT.decode(accessToken, settings.secret);
            const userId = decoded.sub;

            const listNotifications = await NotificationsService.getAllNotifications(userId)();

            return res.status(200).json(listNotifications);
        }
    }

    /* Mark all notification for user as was readed */
    static markAllNotificationsReaded() {

        return async (req, res, next) => {

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;

            const decoded = JWT.decode(accessToken, settings.secret);
            const userId = decoded.sub;

            const result = await NotificationsService.markAllNotificationsReaded(userId)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(500).json(result);
            }
        }
    }

    /* Get count unreaded notifications for user_id */
    static getUnreadNotificationsCount() {

        return async (req, res, next) => {

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;

            // get userId from a accessToken
            const decoded = JWT.decode(accessToken, settings.secret);
            const userId = decoded.sub;

            const result = await NotificationsService.getUnreadNotificationsCount(userId)();

            return res.status(200).json(result);
        }
    }



}

module.exports = NotificationsController;