const JWT = require('jsonwebtoken');
const settings = require('../../settings');
const ProfilesService = require('../services/profiles');


class ProfilesController {

    /* Update client profile info */
    static updateProfileInfo() {

        return async (req, res, next) => {
            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;

            const result = await ProfilesService.updateProfileInfo (accessToken,
                                                                    req.body.username,
                                                                    req.body.email,
                                                                    req.body.first_name,
                                                                    req.body.last_name,
                                                                    req.body.date_of_birth,
                                                                    req.body.phone_number,
                                                                    req.body.password )();
console.log(req.body.date_of_birth);
            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Get user profile info */
    static me() {

        return async (req, res, next) => {

            const accessToken = req.body.accessToken || req.query.accessToken || req.headers.accesstoken;
            const decoded = JWT.decode(accessToken, settings.JWT.clientAccessTokenSecret);
            const findUser = (await ProfilesService.findUser(decoded.sub)())[0];

            if (findUser) {
                
                const tzoffset = (new Date()).getTimezoneOffset() * 60000;      //offset in milliseconds

                return res.status(200).json({
                    'success': true,
                    'id': findUser.id,
                    'name': findUser.name,
                    'first_name': findUser.first_name,
                    'last_name': findUser.last_name,
                    'date_of_birth': findUser.date_of_birth? (new Date(findUser.date_of_birth - tzoffset)).toISOString().slice(0, 10) : null,
                    'phone_number': findUser.phone_number? findUser.phone_number:null,
                    'email': findUser.email
                });

            } else {
                
                return res.status(404).json({
                    'success': false,
                    'message': 'User not found.'
                });  
            }
        }
    }


}

module.exports = ProfilesController;
