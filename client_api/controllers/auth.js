const settings = require('../../settings');
const JWT = require('jsonwebtoken');
const ClientAuthService = require('../services/auth');


class ClientAuthController {

    /* Register new client in table users */
    static clientRegister() {

        return async (req, res, next) => {

            if (! req.body.email) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param email is empty.'
                });
            }

            if (! req.body.password) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param password is empty.'
                });
            }

            if (! req.body.name) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param name is empty.'
                });
            }

            const resultRegister = await ClientAuthService.clientRegister(  req.body.email, 
                                                                            req.body.password, 
                                                                            req.body.name, 
                                                                            req.body.phone_number)();
            if (resultRegister.success !== false) {
                return res.status(200).json(resultRegister);
            } else {
                return res.status(404).json(resultRegister);
            }
        }
    }

    /* Login client by email and password*/
    static clientLogin() {

        return async (req, res, next) => {

            if (! req.body.email) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param email is empty.'
                })
            }

            if (! req.body.password) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param password is empty.'
                })
            }

            const resultLogin = await ClientAuthService.clientLogin(req.body.email, req.body.password)();

            if (resultLogin.success !== false) {
                return res.status(200).json(resultLogin);
            } else {
                return res.status(404).json(resultLogin);
            }
        }
    }

    /* If email exist in database, then send to mail the password recovery link */
    static forgotPassword() {

        return async (req, res, next) => {

            if (! req.body.email) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param email is empty.'
                })
            }

            const result = await ClientAuthService.forgotPassword(req.body.email)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Change password for client if client have a short-lived token*/
    static changePassword() {

        return async (req, res, next) => {

            const shortLivedToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;

            if (! req.body.password) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param password is empty.'
                })
            };

            const result = await ClientAuthService.changePassword(shortLivedToken, req.body.password)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Login client by facebook or register by facebook */
    static facebookLogin() {

        return async (req, res, next) => {

            const facebookShortLivedToken = req.body.short_lived_token;

            if (! facebookShortLivedToken) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param short_lived_token is empty.'
                })
            };

            let facebookToken;
            try {
                facebookToken = await ClientAuthService.getFacebookLongLivedToken(facebookShortLivedToken);
            }
            catch (e) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Facebook token is not valid.'
                });
            }

            const facebookLongLivedToken = facebookToken.access_token;
            const facebookUser = await ClientAuthService.getFacebookUser(facebookLongLivedToken);
            let user = (await ClientAuthService.getUser(facebookUser))[0];
            if (! user) {
                await ClientAuthService.createUser(facebookUser);
                user = (await ClientAuthService.getUser(facebookUser))[0];
            }

            const accessToken = JWT.sign({
                iss: "Seat",
                sub: user.user_id,
                email: user.user_email,
                // exp: new Date().setDate(new Date().getDate() )/1000 + settings.tokenLife
                }, settings.JWT.clientAccessTokenSecret );

            return res.status(200).json({
                'success': true,
                'id': user.user_id,
                'accessToken': accessToken,
                'name': user.user_name,
                'email': user.user_email
            });
        }
    }

    /* Static page with link to reset password*/
    static pageResetPassword() {

        return async (req, res, next) => {

            const token = req.query.token;

            if (! token) {
                const html = `<!DOCTYPE html>
                                <html>
                                    <head>
                                        <meta charset="utf-8" />
                                        <title>SEAT reset password</title>
                                    </head>
                                    <body>
                                        <h1>Reset your password to SEAT FDC account</h1>
                                        <hr>
                                        <br>
                                        <h3 style="color:red" >Access denied, authentication failed.<br>Token is empty.</h3>
                                        <br>
                                        <hr>
                                        SEAT FDC
                                    </body>
                                </html>`;

                res.status(404).write(html);
                res.end();
                return;
            }

            const linkResetPassword = `${settings.linkResetPasswordHtml}?token=${token}`;

            const html = `<!DOCTYPE html>
            <html>
             <head>
              <meta charset="utf-8" />
              <title>SEAT reset password</title>
             </head>
             <body>
             <h1>Reset your password to SEAT FDC account</h1>
             <hr>
             <br>
              <p>You recently requested to reset your password for your SEAT FDC account.<br>
              Click the link below to reset it.<br><br>
              <a href=${linkResetPassword}> Click to reset your password </a> <br><br>
              If you did not request a password reset, please ignore this email or reply to let us know.<br>
              This password reset is only valid for the next 10 minutes.<br><br>
              <br>
              <hr>
              SEAT FDC
             </body>
            </html>
            `;
 
            res.status(200).write(html);
            res.end();
            return;
        }
    }


}

module.exports = ClientAuthController;
