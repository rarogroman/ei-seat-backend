const MailUtils = require('../../app/helpers/mailUtils');

class ClientEmailController {

    /* Send email to seat-fdc */
    static sendEmail() {

        return async (req, res, next) => {

            if (! req.body.name) {
                return res.status(400).json({
                    'success': false,
                    'message': 'Field name is empty.'
                });
            }

            if (! req.body.email) {
                return res.status(400).json({
                    'success': false,
                    'message': 'Field email is empty.'
                });
            }

            if (! req.body.phone) {
                return res.status(400).json({
                    'success': false,
                    'message': 'Field phone is empty.'
                });
            }

            if (! req.body.msg) {
                return res.status(400).json({
                    'success': false,
                    'message': 'Field msg is empty.'
                });
            }

            const result = await MailUtils.sendEmailToSeatFDC(req.body.name, req.body.email, req.body.phone, req.body.msg)();

            if (result.success !== false) {
                return res.status(200).send('<H1>Your email has been sent successfully to seat.fdc@gmail.com.</H1>');
            } else {
                return res.status(500).json(result);
            }
        }
    }


}

module.exports = ClientEmailController;
