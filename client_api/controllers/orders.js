const OrdersService = require('../services/orders');
const JWT = require('jsonwebtoken');
const settings = require('../../settings');



class OrderController {

    /* Create new order in table orders, add products in table order_items */
    static createOrder() {

        return async (req, res, next) => {
console.log('************************ CREATE ORDER **********************');

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            const stadiumId = parseInt(req.body.stadium_id);
console.log(stadiumId);
            if (! stadiumId) {
console.log('no stadium');

                return res.status(404).json({
                    'success': false,
                    'message': 'Param stadium_id is empty.'
                })
            };

            const stanId = parseInt(req.body.stand_id);
            if (! stanId ) {
console.log('no stand');

                return res.status(404).json({

                    'success': false,
                    'message': 'Param stand_id is empty.'
                })
            };

            // get userId from accessToken
            const decoded = JWT.decode(accessToken, settings.secret);
            const userId = decoded.sub;
console.log('userId:', userId);

            const stripeToken = req.body.stripe_token;
            if (! stripeToken) {
console.log('no stripe token');

                return res.status(404).json({

                    'success': false,
                    'message': 'Param stripe_token is empty.'
                })
            };

            const deviceOS = req.body.device_os;
            if ((! deviceOS) || (!(settings.DEVICE_OS.includes(deviceOS))) ) {
                return res.status(404).json({
                    'success': false,
                    'message': `Param device_os is empty or not in [${settings.DEVICE_OS}].`
                })
            };

            const deviceToken = req.body.device_token;
            const delivery = req.body.delivery;
            const deliverySection = req.body.delivery_section;
            const deliveryRow = parseInt(req.body.delivery_row);
            const deliverySeat = parseInt(req.body.delivery_seat);
            const products = req.body.products;

            const orderId = await OrdersService.createOrder(stadiumId,
                                                            stanId,
                                                            userId,
                                                            stripeToken,
                                                            deviceOS,
                                                            deviceToken,
                                                            delivery,
                                                            deliverySection,
                                                            deliveryRow,
                                                            deliverySeat,
                                                            products)();
            if (orderId.success !== false) {
                return res.status(200).json(orderId);
            } else {
                return res.status(500).json(orderId);
            }
        }
    }

    /* Full list orders with detailed info for client_id */
    static getHistoryOrders() {

        return async (req, res, next) => {

            const accessToken = req.body.accessToken || req.query.accessToken || req.headers.accesstoken;
            
            // get userId from a accessToken
            const decoded = JWT.decode(accessToken, settings.secret);
            const userId = decoded.sub;

            const historyOrders = (await OrdersService.getHistoryOrders ( userId )());
            
            if (historyOrders.success !== false) {
                return res.status(200).json(historyOrders);
            } else {
                return res.status(404).json(historyOrders);
            }
        }
    }

    /* Detailed info about order with list of products */
    static getOderDetailed() {

        return async (req, res, next) => {

            const accessToken = req.body.accessToken || req.query.accessToken || req.headers.accesstoken;
            
            // get userId from a accessToken
            const decoded = JWT.decode(accessToken, settings.secret);
            const userId = decoded.sub;
            const orderId = parseInt(req.body.order_id);

            if (! orderId ) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param order_id is empty.'
                })
            };

            const oderDetailed = await OrdersService.getOderDetailed ( userId, orderId )();

            if (oderDetailed.success !== false) {
                return res.status(200).json(oderDetailed);
            } else {
                return res.status(404).json(oderDetailed);
            }
        }
    }

}

module.exports = OrderController;
