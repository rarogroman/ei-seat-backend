const StadiumService = require('../services/stadiums');


class StadiumsController {

    /* Get full list of stadiums */
    static getStadiumsList() {

        return async (req, res, next) => {

            const stadiumsList = await StadiumService.getStadiumsList();
            
            res.status(200).json(stadiumsList);  
        }
    }
    
    /* Get list all stands for one stadium by stadium_id */
    static getStandsList() {

        return async (req, res, next) => {

            const stadiumId = parseInt(req.body.stadium_id);

            if (! stadiumId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param stadium_id is empty.'
                })
            };

            const standsList = await StadiumService.getStandsList(stadiumId);
            
            res.status(200).json(standsList);
        }
    }

}

module.exports = StadiumsController;