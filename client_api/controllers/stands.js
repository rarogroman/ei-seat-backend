const StandsService = require('../services/stands');


class StandsController {

    /* Get list all categories of products for one stand by stand_id */
    static getCategoriesList() {


        return async (req, res, next) => {
            
            const standId = parseInt(req.body.stand_id);

            if (! standId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param stand_id is empty.'
                })
            };

            const categoriesList = await StandsService.getCategoriesListByStandId(standId);
            
            res.status(200).json(categoriesList);
        }
    }

    /* Get list all products for one stand and for one category */
    static getProductListByStandAndCategory() {

        return async (req, res, next) => {

            const standId = parseInt(req.body.stand_id);
            if (! standId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param stand_id is empty.'
                })
            };

            const categoryId = parseInt(req.body.category_id);
            if (! categoryId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param category_id is empty.'
                })
            };

            const productList = await StandsService.getProductListByStandAndCategory(standId, categoryId);
            
            res.status(200).json(productList); 
        }
    }

}

module.exports = StandsController;