module.exports = {

    database: {
         /* amazon */
         mysql: {
            host: "localhost",
            user: "admin",
            password: "lkUy*N-jalsdkfmnva^sdjf345qw+fsl+daTfva7^3ryqaesjkfh-Uas78y!eqwkjhdsafjkh",
            database:  "seat",
            timezone: 'UTC'
         }
    },

    email: {
        service: 'gmail',
        secure: false,
        user: 'seat.fdc@gmail.com',
        password: 'H7vh0+7&!457vdw2'
    },

   emailToSendContactUs: 'seat.fdc@gmail.com',

    JWT: {
        clientAccessTokenSecret: "asfdsdfasf",
        clientRefreshTokenSecret: "asdf048ieowufjsadklvcdsiofa0f",
        vendorAccessTokenSecret: "alksdjf;lasjdfl;asdkjfads;lfj",
        vendorRefreshTokenSecret: "545as4dfsad4f5as4df5asf4d5asd",
        resetPasswordTokenLife: 86400,
        refreshTokenLife: 86400,
        adminApiTokenLife: 86400,
        adminApiSecret: "k95dfj+kas-sdl*kf4jaksfa;dslfkj"
    },

    linkResetPassword: "http://seat-fdc.com/page_reset_password.html",
    linkResetPasswordHtml: "seat://reset_password",

    oauth: {
        facebook: {
            clientId: "1958854684128796", //"1630458070415558", //"1958854684128796",
            secretKey: "d1a73b707a8fd364d14334b1ec5dc1f6" //"0a4a0e00d5b039936bfb9cb249f958e5" //"d1a73b707a8fd364d14334b1ec5dc1f6"
        }
    },

    ORDER_STATUSES: [
                    'PENDING',
                    'DECLINED',
                    'PREPARING',
                    'READY_FOR_COLLECTION',
                    'COLLECTED',
                    'ON_ITS_WAY',
                    'DELIVERED',
                    'NOT_VALID' 
                    ],

    ORDER_PUSH_NOTIFICATIONS: {
                    DECLINED: 'Sorry, your order #ORDNUMBER has been declined.\nTry to make a new order later.', 
                    PREPARING: "Thank you for your order! Readiness time READINESSTIME minutes.\nWe'll let know you when the order is ready.",
                    READY_FOR_COLLECTION: "Your order #ORDNUMBER is ready for collection from kiosk 'KIOSKNUMBER'.\nPlease show in the kiosk barcode your order, click to view order details.",
                    COLLECTED: "Your order #ORDNUMBER has been collected from kiosk 'KIOSKNUMBER'.\nThank you for your purchase, click to view order details.",
                    ON_ITS_WAY: "Your order #ORDNUMBER is on the way.\nPlease expect it in the seсtion 'SECTION', ROWNUMBERth row SEATNUMBERth seat, click to view order details.",
                    DELIVERED: 'Your order #ORDNUMBER has been delivered.\nThank you for your purchase, click to view order details.'
    },

    ORDER_NOTIFICATIONS: {
                    DECLINED: 'Sorry, your order #ORDNUMBER has been declined.\nTry to make a new order later.', 
                    PREPARING: "Thank you for your order #ORDNUMBER, readiness time READINESSTIME minutes.\nWe'll let know you when the order is ready.",
                    READY_FOR_COLLECTION: "Your order #ORDNUMBER is ready for collection from kiosk 'KIOSKNUMBER'.\nPlease show in the kiosk barcode your order.",
                    COLLECTED: "Your order #ORDNUMBER has been collected from kiosk 'KIOSKNUMBER'.\nThank you for your purchase.",
                    ON_ITS_WAY: "Your order #ORDNUMBER is on the way.\nPlease expect it in the seсtion 'SECTION', ROWNUMBERth row SEATNUMBERth seat.",
                    DELIVERED: 'Your order #ORDNUMBER has been delivered.\nThank you for your purchase.'
    },

    DEVICE_OS: [
        'ios',
        'android'
    ]
//    secret: 'supersecret'


}
