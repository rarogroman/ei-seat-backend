const NewsService = require('../services/news');
const CImage = require('../../app/drivers/ftp');
const RenameHelper = require('../../app/helpers/rename');
let util = require("util");

class NewsController {

    /* Get full list of news */
    static getNewsList() {

        return async (req, res, next) => {

            const List = await NewsService.getNewsList();

            let status = false;

            if (List.length) {
                status = true;
            }
        
            res.status(200).json({
                status,
                List
            });  
        }
    }


    /* Get news by id */
    static getNewsById() {

        return async (req, res, next) => {

            const newsId = req.params.id;
            if (! newsId ) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Param id is empty.'
                })
            };

            const news = (await NewsService.getNewsById(newsId))[0];
            
            if (news) {
                return res.status(200).json({news});
            } else {
                return res.status(404).json({"error:": `News with number ${newsId} not found`});
            }
        }
    }


    /* Add one item in table news*/
    static addNews() {

        return async (req, res, next) => {
         
            const newsTitle = req.body.title;
            if (! newsTitle ) {
                return res.status(400).json({
                    'success': false,
                    'message': 'News title is empty.'
                })
            };

            let newsImage = req.body.image;
            if (!newsImage ) {
                return res.status(400).json({
                    'success': false,
                    'message': 'News image is empty.'
                })
            } else {
                req.files.image.name = RenameHelper.reName(req.files.image.name);

                let file = new CImage();

                 file.uploadFile(req.files.image);
            };
            newsImage = 'http://35.178.89.178:3000/' + req.files.image.name;

            const newsDescription = req.body.description;
            if (! newsDescription ) {
                return res.status(400).json({
                    'success': false,
                    'message': 'News description is empty.'
                })
            };

            const result = await NewsService.addNews(newsTitle,newsImage, newsDescription)();

            if (result) {
                return res.status(200).json(result);
            } else {
                return res.status(500).json({
                    'success': false,
                    'message': 'An error occurred while adding news.'
                });
            }
        }
    }
    
    
    /* Delete one item in table news*/
    static deleteNewsById() {

        return async (req, res, next) => {
            const newsDeleteId = parseInt(req.body.id);

            if (! newsDeleteId ) {
                return res.status(400).json({
                    'success': false,
                    'message': 'News delete id is empty.'
                })
            };
            const news = (await NewsService.getNewsById(newsDeleteId))[0];

            let file = new CImage();
            
            file.deleteFile(news.news_image.split("http://35.178.89.178:3000/")[1]);

            const result = await NewsService.deleteNewsById(newsDeleteId)();

            if (result.success == true) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }


    /* Update one item in table news*/
    static updateNewsById() {

        return async (req, res, next) => {
            
            const newsUpdateId = parseInt(req.body.id);

            if (!newsUpdateId ) {
                return res.status(400).json({
                    'success': false,
                    'message': 'News update id is empty.'
                })
            };

            const newsUpdateTitle = req.body.title;
            let newsUpdateImage = "";
            const newsUpdateDescription = req.body.description;
       
            if(req.body.image) {
                let file = new CImage();
      
                
                file.deleteFile(req.body.news_image.split("http://35.178.89.178:3000/")[1]);
  
                req.files.image.name = RenameHelper.reName(req.files.image.name);
                newsUpdateImage =  'http://35.178.89.178:3000/' +req.files.image.name;
                file.uploadFile(req.files.image);
            }

            const result = await NewsService.updateNewsById(newsUpdateId, newsUpdateTitle, newsUpdateImage, newsUpdateDescription)();

            if (result.success == true) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }
    


}

module.exports = NewsController;