const VendorsService = require("../services/vendors");
let util = require("util");

class VendorsController {
    static getVendors() {
        return async (req, res) => {
            const vendor = await VendorsService.getVendors(req.query.stadium_id);

            let status = false;

            if (vendor.length) {
                status = true;
            }

            return res.status(200).send({
                status: status,
                vendor: vendor
            });
        }
    }

    static deleteVendor() {
        return async (req, res) => {

            await VendorsService.deleteVendor(req.body.vendor_id);

            await VendorsService.deleteVendorPermissions(req.body.vendor_id);

            return res.status(200).send({
                message: "Vendor are deleted!"
            });
        }
    }

    static createVendor() {
        return async (req, res) => {
            await VendorsService.createVendor(req.query);

            let data = await VendorsService.newVendorId(req.query.vendor_email);

            await VendorsService.vendorPermissions(data[0].id, req.query.stadium_id);

            let vendor = await VendorsService.getVendors(req.query.stadium_id);

            return res.status(200).send({
                vendor : vendor,
                message: "Vendor are created!"
            });
        }
    }
}

module.exports = VendorsController;