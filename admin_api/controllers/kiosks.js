const KiosksService = require("../services/kiosks");
let  util = require("util");

class KiosksController {

    static getKiosks() {
        return async (req, res) => {
            const kiosks = await KiosksService.getKiosks(req.query.stadium_id);
        
            let status = false;
            if(kiosks.length)
                status = true;

            return res.status(200).send({
                status: status,
                kiosks: kiosks
            });
        }
    }

    static createKiosk() {
        return async (req, res) => {
           await KiosksService.createKiosk(req.query);
            const kiosks = await KiosksService.getKiosks(req.query.stadium_id);

            return res.status(200).send({
                kiosks: kiosks,
                message: "Kiosk created!"
            });
        }
    }

    static deleteKiosk() {
        return async (req, res) => {
            await KiosksService.deleteKiosk(req.body.kiosk_id);

            return res.status(200).send({
                message: "kiosk is deleted!"
            });
        }
    }

    static updateKiosk() {
        return async (req, res) => {
            await KiosksService.updateKiosk(req.query);

            return res.status(200).send({ message: "kiosk is updated!" });
        }
    }
}

module.exports = KiosksController;