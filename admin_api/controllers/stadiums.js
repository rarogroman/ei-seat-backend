const StadiumsService = require("../services/stadiums");
const CImage = require('../../app/drivers/ftp');
const RenameHelper = require('../../app/helpers/rename');
let util = require("util");

class StadiumsController {

    static getStadiums() {
        return async (req, res) => {
console.log(req.query.page);
            const stadiums = await StadiumsService.getStadiums(req.query.page);

            return res.status(200).send({
                stadiums: stadiums
            });
        }
    }

    static getStadiumById() {
        return async (req, res) => {
            let stadium = await StadiumsService.getStadiumById(req.query.stadium_id);

            return res.status(200).send({
                stadium: stadium
            });
        }
    }

    static createStadium() {
        return async (req, res) => {
            let stadium_name = req.body.stadium_name;
            let flag = await StadiumsService.isAlreadyExists(stadium_name);

            if (!flag.length) {
                req.files.T_IMG.name = RenameHelper.reName(req.files.T_IMG.name);

                await StadiumsService.createStadium(req.body.stadium_name, 'http://35.178.89.178:3000/' + req.files.T_IMG.name);

                let file = new CImage();

                await file.uploadFile(req.files.T_IMG);

                const stadiums = await StadiumsService.getStadiums(1);

                return res.status(200).send({
                    stadiums: stadiums,
                    message: "Stadium created!"
                });
            } else {
                return res.status(200).send({ message: "Stadium is alredy exist!" });
            }
        }
    }

    static deleteStadium() {
        return async (req, res) => {
            // Получаю данные стадиона перед удалением (что бы знать имя картинки для удалениея из хранилища)
            const stadium = await StadiumsService.getStadiumById(req.body.stadium_id);

            let file = new CImage();
            

            file.deleteFile(stadium[0].stadium_featured_url.split("http://35.178.89.178:3000/")[1]);

            await StadiumsService.deleteStadium(req.body.stadium_id);
            return res.status(200).send({
                message: "Stadium is delete!"
            });
        }
    }

    static updateStadium() {
        return async (req, res) => {

            if (req.body.name_marker) {
                await StadiumsService.updateStadium(req.body.stadium_name, req.body.stadium_id);
            }

            if (req.body.file_marker) {
                let file = new CImage();

                file.deleteFile(req.body.stadium_featured_url.split("http://35.178.89.178:3000/")[1]);

                req.files.T_IMG.name = RenameHelper.reName(req.files.T_IMG.name);

                file.uploadFile(req.files.T_IMG);

                await StadiumsService.updateStadiumImg('http://35.178.89.178:3000/' + req.files.T_IMG.name, req.body.stadium_id);
            }

            let stadium = await StadiumsService.getStadiumById(req.body.stadium_id);

            return res.status(200).send({
                stadium: stadium,
                message: "User are created!"
            });
        }
    }

    static getStadiumsCount() {
        return async (req, res) => {
            let stadium_count = await StadiumsService.getStadiumsCount();

            res.status(200).send({
                stadium_count: stadium_count
            });
        }
    }
}

module.exports = StadiumsController;
