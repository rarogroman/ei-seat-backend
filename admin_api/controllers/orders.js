const OrdersService = require("../services/orders");
let  util = require("util");

class OrdersController {
    
    static getOrdersCount() {
        return async (req, res) => {
            let orders_count = await OrdersService.getOrdersCount(req.query.stadium_id);
           
            res.status(200).send({
                orders_count : orders_count
            });
        }
    }
    static getOrders() {
        return async (req, res) => {
            const orders = await OrdersService.getOrders(req.query.stadium_id, req.query.page);

            let status = false;
            if(orders.length)
                status = true;
           
            return res.status(200).send({
                status: status,
                orders: orders
            });
        }
    }

    static getOrderItemsById() {
        return async (req, res) => {

            const orderItems = await OrdersService.getOrderItemsById(req.query.order_id);

            return res.status(200).send({
                orderItems: orderItems
            });
        }
    }
    
    static getOrderById() {
        return async (req, res) => {
            const order = await OrdersService.getOrderById(req.query.order_id);

            return res.status(200).send({
                order: order
            });
        }
    }
}

module.exports = OrdersController;