const mysql = require('../../app/drivers/mysql');


class NewsService {

    /* Get full list of news */
    static getNewsList() {
        
        let sql =  `SELECT 
                        news.news_id,
                        news.news_title,
                        news.news_image,
                        news.news_description,
                        news.news_date_created
                    FROM news
                    ORDER BY news.news_id ASC`

        return mysql().query(sql);
    }


    /* Get news by id */
    static getNewsById(newsId) {

        let sql =  `SELECT 
                        news.news_id,
                        news.news_title,
                        news.news_image,
                        news.news_description,
                        news.news_date_created
                    FROM news
                    WHERE news.news_id = ${newsId}
                    LIMIT 1 `

        return mysql().query(sql);
    }


    /* Add one item in table news*/
    static addNews(newsTitle, newsImage, newsDescription) {

        return async () => { 

            const newsDateCreated = new Date().toISOString().slice(0, 19).replace('T', ' ');
         
            let sql = `INSERT INTO news (
                            news.news_title,
                            news.news_image,
                            news.news_description,
                            news.news_date_created )
                        VALUES (
                            '${newsTitle}',
                            '${newsImage}',
                            '${newsDescription}',
                            '${newsDateCreated}' )`;

            let updateResult = await mysql().query(sql);
     
            if (updateResult.affectedRows) {
                // if the news was created in table news, than get the news_id
                sql  = `SELECT
                            news_id,
                            news_title,
                            news_image,
                            news_description
                        FROM news
                        ORDER BY news.news_id ASC`;

                const newsInfo = await mysql().query(sql);
         
                return {
                    'success': true,
                    'message': 'The news was added successfully.',
                    'news': newsInfo
                    };

            } else {
                return false;
            }
        }
    }


    /* Delete one item in table news*/
    static deleteNewsById(newsDeleteId) {

        return async () => { 

            let sql =  `DELETE 
                        FROM
                            news
                        WHERE
                            news_id = '${newsDeleteId}' `;

            let result = await mysql().query(sql);

            if (result.affectedRows) {

                return {
                    'success': true,
                    'message': 'The news was deleted successfully.'
                    };

            } else {
                return {
                    'success': false,
                    'message': `News with id = ${newsDeleteId} not exist in database.`
                    };
            }
        }
    }

    /* Update one item in table news*/
    static updateNewsById(newsUpdateId, newsUpdateTitle, newsUpdateImage, newsUpdateDescription) {

        return async () => { 

            // All fields are empty. Nothing to update
            if (!(newsUpdateTitle || newsUpdateImage || newsUpdateDescription)) {
                return {
                    'success': false,
                    'message': 'All fields are empty. Nothing to update.'
                    };
            }

            let sql =  'UPDATE news SET ';

            // Optional field request - title
            if (newsUpdateTitle) {
                sql += `news_title='${newsUpdateTitle}',`
            }

            // Optional field request - image
            if (newsUpdateImage) {
                sql += `news_image='${newsUpdateImage}',`
            }

            // Optional field request - description
            if (newsUpdateDescription) {
                sql += `news_description='${newsUpdateDescription}',`
            }

            sql = sql.slice(0,-1) + ` WHERE news_id = '${newsUpdateId}' `;
            
            let result = await mysql().query(sql);

            if (result.affectedRows) {

                return {
                    'success': true,
                    'message': 'The news was updated successfully.'
                    };

            } else {
                return {
                    'success': false,
                    'message': `News with id = ${newsUpdateId} not exist in database.`
                    };
            }
        }
    }

}

module.exports = NewsService;