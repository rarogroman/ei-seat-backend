const mysql = require("../../app/drivers/mysql");

class ProductsService {

    static uploadProdList(product_id, kiosk_id) {
        let sqlInsertItems = 'INSERT INTO warehouses (kiosk_id, product_id, warehouse_quantity, warehouse_enable) VALUES ';
        for (const item of Object.keys(product_id)) {
            sqlInsertItems += ` ('${kiosk_id}', '${product_id[item]}', '${200}', '${1}' ),`;
        }

        sqlInsertItems = sqlInsertItems.slice(0, -1);
        return mysql().query(sqlInsertItems);
    }

    static eraseProdList(product_id, kiosk_id) {
        let sqlDeleteItems = 'DELETE FROM `warehouses` WHERE (product_id, kiosk_id) IN (';
        for (const item of Object.keys(product_id)) {
            sqlDeleteItems += ` ('${product_id[item]}', '${kiosk_id}'),`;
        }

        sqlDeleteItems = sqlDeleteItems.slice(0, -1);
        sqlDeleteItems +=')'
        return mysql().query(sqlDeleteItems);
    }

    // Удалить Product
    static deleteProduct(product_id) {
        //удалить картинку
        let sql = "DELETE FROM `products` WHERE ?";

        return mysql().query(sql, [{
            product_id: parseInt(product_id)
        }]);
    }

    // Создать продукт
    static createProduct(data) {
        let sql = "INSERT INTO `products` SET ?";

        return mysql().query(sql, {
            product_name: data.product_name,
            category_id: data.category_id,
            product_price: data.product_price
            // product_description    : data.product_description,
        });
    }

    // Проверка на существование продукта
    static isAlreadyExists(product_name) {

        let sql = "SELECT product_id FROM `products` WHERE ?";

        return mysql().query(sql, [{
            product_name: product_name
        }]);
    }

    static getProductsById(kiosk_id) {
        let sql = "SELECT product_id FROM `warehouses` WHERE kiosk_id = " + parseInt(kiosk_id);

        return mysql().query(sql);
    }

    static getAllProducts() {
        let sql = "SELECT product_id, product_name FROM `products` ";

        return mysql().query(sql);
    }

    // Вернуть Product по дате создания
    static getProducts(page) {
        let sql = "SELECT * FROM `products` ";
        sql += "ORDER BY product_id DESC "
        sql += "LIMIT 10 OFFSET " + (parseInt(page) - 1) * 10;

        return mysql().query(sql);
    }

    // Вернуть число стадионов
    static getProductsCount() {
        let sql = "SELECT COUNT(*) AS product_count FROM `products` ";

        return mysql().query(sql);
    }

    // Обновить Product
    static updateProduct(data) {
        let sql = "UPDATE `products` SET ? WHERE ?";

        return mysql().query(sql, [
            { // set value
                category_id:      data. category_id,
                product_price:    data.product_price,
                product_name:     data.product_name
            },
            { // where
                product_id: parseInt(data.product_id)
            }
        ]);
    }

}

module.exports = ProductsService;