const bcrypt = require('bcryptjs');
const mysql = require("../../app/drivers/mysql");

class VendorsService {

    static getVendors(stadium_id) {
        let sql = "SELECT vendors.vendor_id, vendors.vendor_name, vendors.vendor_email FROM `vendors` ";
        sql += " INNER JOIN vendor_permissions ON vendors.vendor_id = vendor_permissions.vendor_id ";
        sql += " WHERE vendor_permissions.stadium_id = '" + stadium_id + "' ";
        return mysql().query(sql);
    }

    static createVendor(data) {


        var salt = bcrypt.genSaltSync(10);
        data.vendor_password = bcrypt.hashSync(data.vendor_password, salt);

        let sql = `INSERT INTO vendors (
            vendors.vendor_name,
            vendors.vendor_email,
            vendors.vendor_password
            )
        VALUES (
            '${data.vendor_name}',
            '${data.vendor_email}',
            '${data.vendor_password}'
            )`;
        return mysql().query(sql);
    }

    static newVendorId(vendor_email) {
        let sql = `SELECT
        vendors.vendor_id AS id
        FROM
        vendors
        WHERE
        vendors.vendor_email = '${vendor_email}'
        LIMIT 1`;
        return mysql().query(sql);
    }

    static vendorPermissions(vendor_id, stadium_id) {

        let sql = `INSERT INTO vendor_permissions (
            vendor_permissions.vendor_id, 
            vendor_permissions.stadium_id
            )
        VALUES (
            '${vendor_id}',
            '${stadium_id}'
            )`;

        return mysql().query(sql);
    }

    static deleteVendor(vendor_id) {
        let sql = `DELETE FROM vendors WHERE vendor_id = '${vendor_id}'
                   `;
        return mysql().query(sql);
    }

    static deleteVendorPermissions(vendor_id) {
        let sql = `DELETE FROM vendor_permissions WHERE vendor_id = '${vendor_id}'
                   `;
        return mysql().query(sql);
    }
}

module.exports = VendorsService;
