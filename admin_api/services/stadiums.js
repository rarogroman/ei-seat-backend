const mysql = require("../../app/drivers/mysql");
const CImage = require('../../app/drivers/ftp');
const RenameHelper = require('../../app/helpers/rename');


class StadiumsService {

    // Update  stadium name
    static updateStadium(stadium_name, stadium_id) {
        let sql = "UPDATE stadiums SET ? WHERE ?";

            return mysql().query(sql, [
                { // set value
                    stadium_name           : stadium_name,
                },
                { // where
                    stadium_id: parseInt(stadium_id)
                }
            ]);
    }

    // Update  stadium img
    static updateStadiumImg(stadium_featured_url, stadium_id) {
        let sql = "UPDATE stadiums SET ? WHERE ?";

        return mysql().query(sql, [
            { // set value
                stadium_featured_url           : stadium_featured_url,
            },
            { // where
                stadium_id: parseInt(stadium_id)
            }
        ]);
    }

    // Удалить стадион
    static deleteStadium(stadium_id) {
        //удалить картинку
        let sql = "DELETE FROM `stadiums` WHERE ?";

        return mysql().query(sql, [{
            stadium_id: parseInt(stadium_id)
        }]);
    }

    // Создать стадион 
    static createStadium(stadium_name, img_name) {
        let sql = "INSERT INTO `stadiums` SET ?";
        
        return mysql().query(sql, {
            stadium_name            : stadium_name,
            stadium_featured_url    : img_name,
            city_id                 : 1,
        });
    }

    // Проверка на существование стадиона
    static isAlreadyExists(stadium_name) {

        let sql = "SELECT stadium_id FROM `stadiums` WHERE ?";

        return mysql().query(sql, [{
            stadium_name: stadium_name
        }]);
    }

    // Вернуть стадион по id
    static getStadiumById(stadium_id) {
        let sql = "SELECT * FROM `stadiums` WHERE stadium_id = " + parseInt(stadium_id) + " LIMIT 1";

        return mysql().query(sql);
    }

    // Вернуть стадионы
    static getStadiums(count = 1) {
console.log('--------- get stadiums');
console.log('count: ', count);
        let sql;
        if (count === 'all') {
        sql = "SELECT * FROM `stadiums` ";
        sql += "ORDER BY stadium_id DESC";

	} else {
        sql = "SELECT * FROM `stadiums` ";
        sql += "ORDER BY stadium_id DESC "
        sql += "LIMIT 10 OFFSET " + (parseInt(count) - 1) * 10;
	}
console.log(sql);
        return mysql().query(sql);
    }

    // Вернуть число стадионов
    static getStadiumsCount() {
        let sql = "SELECT COUNT(*) AS stadium_count FROM stadiums ";

        return mysql().query(sql);
    }
}

module.exports = StadiumsService;
