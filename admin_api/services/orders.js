const mysql = require("../../app/drivers/mysql");

class OrdersService {

    static getOrders(stadium_id, page) {
        let sql = "SELECT * FROM `orders` WHERE orders.order_stadium_id = '"+stadium_id+"' ";
        sql += "ORDER BY orders.order_id DESC "
        sql += "LIMIT 10 OFFSET " + (parseInt(page) - 1) * 10;

        return mysql().query(sql);
    }

    static getOrderItemsById(order_id, product_id) {
        let sql = "SELECT ";
        sql +=" order_items.order_item_quantity, ";
        sql +=" order_items.order_item_price,";
        sql +=" products.product_name";
        sql +=" FROM `order_items`";
        sql +=" INNER JOIN products ON products.product_id = order_items.product_id ";
        sql +=" WHERE order_items.order_id = '"+ order_id +"'";
    

        return mysql().query(sql);
    }

    static getOrderById(order_id) {
        let sql = "SELECT ";
        sql +=" users.user_name AS client_name,";
        sql +=" users.user_phone_number AS client_phone,";
        sql +=" orders.order_delivery AS delivery,";
        sql +=" orders.order_delivery_section,";
        sql +=" orders.order_delivery_row,";
        sql +=" orders.order_delivery_seat,";
        sql +=" orders.order_date_created AS created,";
        sql +=" orders.order_readiness_time,";
        sql +="  orders.order_status,";
        sql +=" Sum(order_item_quantity*order_item_price ) AS total_price";
        sql +=" FROM `orders`";
        sql +=" INNER JOIN order_items ON orders.order_id = order_items.order_id ";
        sql +=" INNER JOIN users ON orders.user_id = users.user_id ";
        sql +=" WHERE orders.order_id = '"+order_id+"'";

        return mysql().query(sql);
    }

     // Вернуть число ордеров
     static getOrdersCount(stadium_id) {
        let sql = "SELECT COUNT(*) AS orders_count FROM `orders` WHERE orders.order_stadium_id = '"+stadium_id+"'";

        return mysql().query(sql);
    }
}

module.exports = OrdersService;