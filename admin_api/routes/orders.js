const OrdersController = require("../controllers/orders");
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const verifyToken = require('../middlewares/verifyToken');

router.get('/getorders',
    verifyToken,
    asyncHandler(OrdersController.getOrders()));

router.get('/getorderbyid',
    verifyToken,
    asyncHandler(OrdersController.getOrderById()));

//Orders/getOrdersCount 
router.get('/getorderscount',
    verifyToken,
    asyncHandler(OrdersController.getOrdersCount()));

//Orders/getOrderItemsById 
router.get('/getorderitemsbyid',
    verifyToken,
    asyncHandler(OrdersController.getOrderItemsById()));

module.exports = router;