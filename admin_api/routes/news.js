const express = require('express');
const router = express.Router();
const NewsController = require('../controllers/news');
const asyncHandler = require('express-async-handler');
const verifyToken = require('../middlewares/verifyToken');

router.get('/all',
    asyncHandler(NewsController.getNewsList())
);

router.get('/:id',
    asyncHandler(NewsController.getNewsById())
);

router.post('/add',
    verifyToken,
    asyncHandler(NewsController.addNews())
);

router.post('/delete',
    verifyToken,
    asyncHandler(NewsController.deleteNewsById())
);

router.post('/update',
    verifyToken,
    asyncHandler(NewsController.updateNewsById())
);




module.exports = router;
