const ProductsController = require("../controllers/products");
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const verifyToken = require('../middlewares/verifyToken');

//Products/getProducts 
router.get('/getproducts',
    verifyToken,
    asyncHandler(ProductsController.getProducts()));

//Products/getProductsCount createProduct deleteProduct
router.get('/getproductscount',
    verifyToken,
    asyncHandler(ProductsController.getProductsCount()));

//Products/getProductById
router.get('/getallproducts',
    verifyToken,
    asyncHandler(ProductsController.getAllProducts()));

router.get('/getproductsbyid',
    verifyToken,
    asyncHandler(ProductsController.getProductsById()));

//Products/createProduct
router.post('/createproduct',
    verifyToken,
    asyncHandler(ProductsController.createProduct()));

//Products/deleteProduct
router.post('/deleteproduct',
    verifyToken,
    asyncHandler(ProductsController.deleteProduct()));

//Products/updateProduct
router.post('/updateproduct',
    verifyToken,
    asyncHandler(ProductsController.updateProduct()));

//Products/updateProduct
router.post('/eraseprodlist',
    verifyToken,
    asyncHandler(ProductsController.eraseProdList()));

//Products/updateProduct
router.post('/uploadprodlist',
    verifyToken,
    asyncHandler(ProductsController.uploadProdList()));


module.exports = router;