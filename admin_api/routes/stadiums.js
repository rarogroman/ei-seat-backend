const StadiumsController = require("../controllers/stadiums");
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const verifyToken = require('../middlewares/verifyToken');

//Stadiums/getStadiumss 
router.get('/getstadiums',
    verifyToken,
    asyncHandler(StadiumsController.getStadiums()));

//Stadiums/StadiumssCount 
router.get('/getstadiumscount',
    verifyToken,
    asyncHandler(StadiumsController.getStadiumsCount()));

//Stadiums/StadiumById
router.get('/getstadiumbyid',
    verifyToken,
    asyncHandler(StadiumsController.getStadiumById()));

//Stadiumss/createStadiums
router.post('/createstadium',
    verifyToken,
    asyncHandler(StadiumsController.createStadium()));

//Stadiums/deleteStadiums
router.post('/deletestadium',
    verifyToken,
    asyncHandler(StadiumsController.deleteStadium()));

//Stadiums/updateStadiums
router.post('/updatestadium',
    verifyToken,
    asyncHandler(StadiumsController.updateStadium()));

module.exports = router;