const VendorsController = require("../controllers/vengors");
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const verifyToken = require('../middlewares/verifyToken');

router.get('/getvendor',
    verifyToken,
    asyncHandler(VendorsController.getVendors()));

router.get('/createvendor',
    verifyToken,
    asyncHandler(VendorsController.createVendor()));

router.post('/deletevendor',
    verifyToken,
    asyncHandler(VendorsController.deleteVendor()));

module.exports = router;