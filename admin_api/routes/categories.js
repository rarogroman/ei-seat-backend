const CategoriesController = require("../controllers/categories");
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const verifyToken = require('../middlewares/verifyToken');

//Categories/getCategories 
router.get('/getcategories',
    verifyToken,
    asyncHandler(CategoriesController.getCategories()));

//categories/createCategory
router.get('/createcategory',
    verifyToken,
    asyncHandler(CategoriesController.createCategory()));

//categories/deleteCategory
router.post('/deletecategory',
    verifyToken,
    asyncHandler(CategoriesController.deleteCategory()));

//categories/updateCategory
router.post('/updatecategory',
    verifyToken,
    asyncHandler(CategoriesController.updateCategory()));

module.exports = router;