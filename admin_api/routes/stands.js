const StandsController = require("../controllers/stands");
const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const verifyToken = require('../middlewares/verifyToken');

//Stadiums/getStadiumss 
router.get('/getstands',
    verifyToken,
    asyncHandler(StandsController.getStands()));

//Stadiumss/createStadiums
router.get('/createstand',
    verifyToken,
    asyncHandler(StandsController.createStand()));

//Stadiums/deleteStadiums
router.post('/deletestand',
    verifyToken,
    asyncHandler(StandsController.deleteStand()));

//Stadiums/updateStadiums
router.post('/updatestand',
    verifyToken,
    asyncHandler(StandsController.updateStand()));

module.exports = router;