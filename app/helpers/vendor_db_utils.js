const mysql = require('../../app/drivers/mysql');


class VendorDbUtils {

    /* Return the list of stadiums for vendor where wendor have permissions */
    static getVendorPermissions ( vendorId ) {

        return async (req, res, next) => {

            const sql  =   `SELECT
                                stadium_id AS stadium_id
                            FROM
                                vendor_permissions
                            WHERE
                                vendor_permissions.vendor_id = '${vendorId}' `;

            const vendorPermissions = await mysql().query(sql);
            let listPermissions = [];
            let keys = Object.keys(vendorPermissions);
            keys.forEach(function(key){
                listPermissions.push(parseInt(vendorPermissions[key].stadium_id));
            });

            return listPermissions;
        }
    }


    /* Return the list of kiosks for vendor where wendor have permissions */
    static getVendorPermissionsKiosks ( vendorId ) {

        return async (req, res, next) => {

            const sql  =`SELECT
				kiosks.kiosk_id
			FROM
				vendor_permissions
			INNER JOIN stands ON vendor_permissions.stadium_id = stands.stadium_id
			INNER JOIN kiosks ON kiosks.stand_id = stands.stand_id
			WHERE
				vendor_permissions.vendor_id = '${vendorId}' `;

            const vendorPermissions = await mysql().query(sql);
            let listPermissions = [];
            let keys = Object.keys(vendorPermissions);
            keys.forEach(function(key){
                listPermissions.push(parseInt(vendorPermissions[key].kiosk_id));
            });

            return listPermissions;
        }
    }



    /* Return the list of orders for vendorId */
    static getVendorOrders ( vendorId ) {

        return async (req, res, next) => {
        
            // get the list of vendor orders
            const sql = `SELECT 
                            orders.order_id AS id
                        FROM 
                            vendor_permissions 
                        INNER JOIN 
                            orders ON vendor_permissions.stadium_id = orders.order_stadium_id
                        WHERE vendor_permissions.vendor_id = ${vendorId};`;

            const vendorListOrders = await mysql().query(sql);

            let listOrders = [];
            let keys = Object.keys(vendorListOrders);
            keys.forEach(function(key){
                listOrders.push(parseInt(vendorListOrders[key].id));
            });

            return listOrders;
        }
    }


 
}

module.exports = VendorDbUtils;
