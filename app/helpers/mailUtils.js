const settings = require('../../settings');
const nodemailer = require('nodemailer');
const dateformat = require('dateformat');


class MailUtils {

    /* Send mail with conteng html */
    static sendMail(email, htmlContent, subject) {

        return async () => { 

            // configuration the mail service
            let smtpConfig = {
                service: settings.email.service,
                secure: settings.email.secure,
                auth: {
                    user: settings.email.user,
                    pass: settings.email.password
                },
                tls: {
                    rejectUnauthorized: false
                }
            };
            const transporter = nodemailer.createTransport(smtpConfig);

            let mail = {
                from: "SEAT FDC<seat.fdc@gmail.com>",
                to: email,
                subject: subject,
                html: htmlContent
            };

            // send the email
            let err = '',
                resultMail = '';

            try {
                resultMail = await transporter.sendMail(mail);
            } catch (err) {
                return {
                    'success': false,
                    'message': 'An error occurred while sending email',
                    'detail': err
                };
            } finally {
                transporter.close();
            }

            // no error during sending the email
            return {
                'success': true,
                'messate': 'Email was sent successfully',
                'detail': resultMail
            };
        }
    }

    /* Send the email to client that the order was created */
    static sendEmailAboutCreatedOrder(order) {

        return async () => { 

            if (order.customer.email) {
                const htmlContent = await this.getHtmlContentForEmail(order)();
                this.sendMail(order.customer.email, htmlContent, 'Thank you for uour purchase!')();
            }
        }
    }

    /* Send the email to client that the order was created */
    static getHtmlContentForEmail(order) {

        return async () => { 

            const estimatedTimeString = dateformat((new Date(new Date(order.created).getTime() + order.readiness_time*60000)), 'HH:MM');

            let html = `
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>Seat Mail</title>
                        <!--[if (gte mso 9)|(IE)]>
                        <style type="text/css">
                        table {border-collapse: collapse;}
                        </style>
                        <![endif]-->
                    </head>
                    <body>
                        <center class="wrapper">
                            <div class="webkit">
                                <!--[if (gte mso 9)|(IE)]>
                                <table width="600" align="center">
                                <tr>
                                <td>
                                <![endif]-->
                                <table class="outer" align="center" style="border-spacing: 0; border: none; width: 100%;" >
                                    <tr>
                                        <td cellpadding="10" cellspacing="0" style="border: 2px solid #000; text-align: center; vertical-align: middle; height: 100px;">
                                            <a href="#" style="display: inline-block; width: 80px; height: 80px; text-decoration: none;">
                                                <img src="logo.png" width="80" height="80" alt="logo" style="display: inline-block; outline: none; border:none;" />    
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td cellpadding="10" cellspacing="0" style="border:none; text-align: center; vertical-align: middle; height: 70px;">
                                            <img src="icon_confirmation.png" width="50" height="50" alt="confirmed" style="display: inline-block; outline: none; border:none;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td cellpadding="10" cellspacing="0" style="border:none;">
                                            <table align="center" style="border-spacing: 0; border: none; width: 100%;">
                                                <tr>
                                                    <td cellpadding="0" cellspacing="0" style="border:none; text-align: center;">
                                                        <p class="h1" style="font: 600 italic 31px/35px 'Arial', serif; color: #000000; width: 100%; margin: 15px 0;">We got your order</p>
                                                        <p class="text" style="font: 21px/25px 'Arial', sans-serif; color: #000000; width: 100%; margin: 25px 0;">Your order has been received by the stadium '${order.stadium}' and will be with in no time!</p>
                                                        <p class="text" style="font: 21px/25px 'Arial', sans-serif; color: #000000; width: 100%; margin: 25px 0;">Your estmated collecton/Delivery time is <span>${estimatedTimeString}</span></p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td cellspacing="0" style="padding: 25px 10px 10px; border:none; text-align: center; font: 26px/30px 'Arial', sans-serif; color: #000000;">Order #${order.order_id}</td>
                                    </tr>
                                    <tr>
                                        <td cellspacing="0" style="padding: 20px 40px 20px 40px; border:none; text-align: center;">
                                            <table class="items" style="border-spacing: 0; border: none; width: 100%;">
                                            #ORDER_ITEMS
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td cellspacing="0" style="padding: 10px 20px 10px 20px; border:none; text-align: center;">
                                            <table class="items" style="border-spacing: 0; border: none; width: 100%;">
                                                <tr>
                                                    <td cellpadding="10" cellspacing="0" style="padding: 10px 10px; border:none; text-align: left; font: 31px/35px 'Arial', serif; color: #000000; width: 50%;">Total</td>
                                                    <td class="total" cellpadding="10" cellspacing="0" style="padding: 10px 10px; border:none; text-align: right; font: 31px/35px 'Arial', serif; color: #000000; width: 50%;">&#163;${order.total_price}</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td cellpadding="10" cellspacing="0" style="padding: 35px 10px 15px 10px; border:none; text-align: center; vertical-align: middle; height: 70px;">
                                            <a style="display: inline-block; margin: 0 10px 0 10px; text-decoration: none;" href="http://instagram.com"><img width="40" height="35" src="icon_instagram.png" alt="instagram" style="display: inline-block; outline: none; border:none;" /></a>
                                            <a style="display: inline-block; margin: 0 10px 0 10px; text-decoration: none;" href="http://facebook.com"><img width="40" height="35" src="icon_facebook.png" alt="facebook" style="display: inline-block; outline: none; border:none;" /></a>
                                            <a style="display: inline-block; margin: 0 10px 0 10px; text-decoration: none;" href="http://twitter.com"><img width="40" height="35" src="icon_twitter.png" alt="twitter" style="display: inline-block; outline: none; border:none;" /></a>
                                            <a style="display: inline-block; margin: 0 10px 0 10px; text-decoration: none;" href="http://youtube.com"><img width="55" height="35" src="icon_youtube.png" alt="youtube" style="display: inline-block; outline: none; border:none;" /></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td cellpadding="10" cellspacing="0" style="padding: 10px 10px 10px 10px; border:none; text-align: center; vertical-align: middle;">
                                            <p style="text-align: center; font: 14px/18px 'Arial', sans-serif; color: #000000; margin: 15px 0;">If there are any issues regarding your food or you fnish to speak to us about our service please contact us on</p>
                                            <a href="mailto:info@seat-fdc.co.uk" style="font: 14px/18px 'Arial', sans-serif; color: #1370c9; text-decoration: underline; margin: 15px 0;">info@seat-fdc.co.uk</a>
                                            <p style="text-align: center; font: 14px/18px 'Arial', sans-serif; color: #000000; margin: 15px 0;">Please note Collecton/Delivery tme are subject to change</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td cellpadding="10" cellspacing="0" style="padding: 5px 10px 95px 10px; border:none; text-align: center; vertical-align: middle;">
                                            <table style="border-spacing: 0; border: none; width: 100%;">
                                                <tr>
                                                    <td cellpadding="10" cellspacing="0" style="padding: 5px 20px 5px 10px; border:none; text-align: right; vertical-align: middle;"><a href="#" style="font: 14px/18px 'Arial', sans-serif; color: #000000; text-decoration: underline; margin: 15px 0;">Terms</a></td>
                                                    <td cellpadding="10" cellspacing="0" style="padding: 5px 10px 5px 20px; border:none; text-align: left; vertical-align: middle;"><a href="#" style="font: 14px/18px 'Arial', sans-serif; color: #000000; text-decoration: underline; margin: 15px 0;">Privacy</a></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <!--[if (gte mso 9)|(IE)]>
                                </td>
                                </tr>
                                </table>
                                <![endif]-->
                            </div>
                        </center>
                    </body>
                </html>
            
            
            `;

            let orderItems = '';
            for (const item of Object.keys(order.products)) {
                orderItems += `
                    <tr>
                        <td cellpadding="10" cellspacing="0" style="padding: 10px 10px; border:none; text-align: left; font: 14px/18px 'Arial', sans-serif; color: #000000; width: 50%;">${order.products[item].quantity}? ${order.products[item].name}</td>
                        <td cellpadding="10" cellspacing="0" style="padding: 10px 10px; border:none; text-align: right; font: 14px/18px 'Arial', sans-serif; color: #000000; width: 50%;">&#163;${order.products[item].price}</td>
                    </tr>
                `;
            }

            return html.replace('#ORDER_ITEMS', orderItems);
        }
    }

    /* Send email from url http://seat-fdc.com/contact_us.html to seat-fdc */
    static sendEmailToSeatFDC(name, email, phone, msg) {

        return async () => { 
            const subject = 'This email was sent from the feedback form http://seat-fdc.com/contact_us';

            // configuration the mail service
            let smtpConfig = {
                service: settings.email.service,
                secure: settings.email.secure,
                auth: {
                    user: settings.email.user,
                    pass: settings.email.password
                },
                tls: {
                    rejectUnauthorized: false
                }
            };
            const transporter = nodemailer.createTransport(smtpConfig);
            const emailToSendContactUs = settings.emailToSendContactUs;

            let html = `
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>${subject}</title>
                    </head>
                    <body>
                        <H3>${subject}</H3>
                        <hr>
                        <center class="wrapper">
                            <div class="webkit">
                                <H4 align="left"><ins>Customer Information:</ins></H4>
                                <p align="left" class="text" style="font: 12px 'Arial', sans-serif; width: 100%; margin: 25px 0;">Name: ${name}</p>
                                <p align="left" class="text" style="font: 12px 'Arial', sans-serif; width: 100%; margin: 25px 0;">Email: ${email}</p>
                                <p align="left" class="text" style="font: 12px 'Arial', sans-serif; width: 100%; margin: 25px 0;">Phone: ${phone}</p>
                                <hr>
                                <br>
                                <br>
                                <H4 align="left"><ins>Message text:</ins></H4>
                                <p align="left" class="text" style="border: 1px solid; border-color: #66A0F7; font: 16px 'Arial', sans-serif; width: 96%; padding:20px;">${msg}</p>
                                <br>
                        </center>
                    </body>
                </html>
            `;


            let mail = {
                from: `${name}<${email}>`,
                to: emailToSendContactUs,
                subject: subject,
                html: html
            };

            // send the email
            let err = '',
                resultMail = '';

            try {
                resultMail = await transporter.sendMail(mail);

            } catch (err) {
                return {
                    'success': false,
                    'message': 'An error occurred while sending email',
                    'detail': err
                };
            } finally {
                transporter.close();
            }

            // no error during sending the email
            return {
                'success': true,
                'messate': 'Email was sent successfully',
                'detail': resultMail
            };
        }
    }
 
}

module.exports = MailUtils;
