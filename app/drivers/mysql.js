const mysql = require('mysql');
const settings = require('../../settings');

var mysqlDriver = (function() {

    var instance;

    function init() {
        var pool = mysql.createPool({
            host     :  settings.database.mysql.host,
            user     :  settings.database.mysql.user,
            password :  settings.database.mysql.password,
            database :  settings.database.mysql.database,
            timezone :  settings.database.mysql.timezone
        });

        return {
            query: function(sql, props) {
                return new Promise(function (resolve, reject) {
                    pool.getConnection(function(err, connection) {

                        try {
                            connection.query(sql, props, function(err, result) {
                                if (err) {
                                    // dev
                                    reject({
                                        message: "dev_mode: " + err.message
                                    });
                                    
                                    // prod
                                    // reject({
                                    //     message: "Error db query!"
                                    // });
                                } else {
                                    resolve(result);
                                }
                            });
    
                            connection.release();
                        } catch(error) {
                            reject({
                                message: "Error db connection!"
                            });
                        }
                    });
                });
            }
        };
    };

    return {
        getInstance: function() {
            if (! instance) {
                instance = init();
            }
            return instance;
        }
    };

})();

module.exports = mysqlDriver.getInstance;