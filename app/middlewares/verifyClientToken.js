const settings = require('../../settings');
const JWT = require('jsonwebtoken');

module.exports = function(req,res,next) {

    let token = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
    if (token) {
        // verifies secret and checks exp
        JWT.verify(token, settings.JWT.clientAccessTokenSecret, function(err, decoded) {
            if (err) {                                      // failed verification
                if (err.name == "JsonWebTokenError") {
                    return res.status(404).send({
                        'success': false,
                        'error': 'Access forbidden, accessToken is not valid.'
                    });                    
                } else if (err.name == "TokenExpiredError") {
                    return res.status(404).send({
                        'success': false,
                        'error': 'Authorization time expired. Please update the link with access token.'
                    });   
                }
                else {
                    return res.status(404).send({
                        'success': false,
                        'error': 'Access forbidden, ' + err.name + '.'
                    });
                } 
            }
            req.decoded = decoded;
            next();                         //no error, proceed
        });
    } else {
        // forbidden without token
        return res.status(404).send({
            'success': false,
            'error': 'Access forbidden, accessToken not exist.'
        });
    }
}