const mysql = require('../../app/drivers/mysql');
const JWT = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const settings = require('../../settings');


class VendorAuthService {

    /*====================================
    Register new vendor in table vendors
    with not permissions
    ===================================*/
    static vendorRegister(email, password, name){

        return async () => { 

            // vendor with such an email already exists in the database ?
            let sql  = `SELECT
                            vendors.vendor_email AS email, 
                            vendors.vendor_password AS password
                        FROM
                            vendors 
                        WHERE
                            vendors.vendor_email = '${email}'
                        LIMIT 1`;

            let dbVendor = (await mysql().query(sql))[0];

            if (dbVendor) {
                // such vendor is already registered
                return {
                    'success': false,
                    'message': 'This email is already registered.'
                };
            } else {
                // new vendor proceed registration
                const hashedPassword = bcrypt.hashSync(password, 10);

                let sql  = `INSERT INTO vendors (
                                vendors.vendor_name,
                                vendors.vendor_email, 
                                vendors.vendor_password
                                )
                            VALUES (
                                '${name}',
                                '${email}',
                                '${hashedPassword}'
                                )`;

                const successNewVendor = await mysql().query(sql);

                if (successNewVendor) {
                    let sql  = `SELECT
                                    vendors.vendor_id AS id,
                                    vendors.vendor_name AS name,
                                    vendors.vendor_email AS email
                                FROM
                                    vendors 
                                WHERE
                                    vendors.vendor_email = '${email}' AND 
                                    vendors.vendor_password = '${hashedPassword}'
                                LIMIT 1`;

                    const dbVendor = (await mysql().query(sql))[0];

                    // get accessToken
                    const accessToken = JWT.sign({
                        iss: "Seat",
                        sub: dbVendor.id,
                        email: dbVendor.email,
                        // exp: new Date().setDate(new Date().getDate() )/1000 + settings.tokenLife
                        }, settings.JWT.vendorAccessTokenSecret);

                        return {
                            'success': true,
                            'message': 'New vendor successfully registered.',
                            'id': dbVendor.id,
                            'accessToken': accessToken,
                            'name': dbVendor.name,
                            'email': dbVendor.email
                        }
                } else {
                    return {
                        'success': false,
                        'message': 'Registration failed.'
                    };
                }
            }
        }
    }


    static vendorLogin(email, password) {
    /*===============================
    Login vendor, return accessToken
    ================================*/
        return async () => { 

            let sql  = `SELECT
                            vendors.vendor_id AS id,
                            vendors.vendor_email AS email, 
                            vendors.vendor_password AS password,
                            vendors.vendor_name AS name
                        FROM
                            vendors 
                        WHERE
                            vendors.vendor_email = '${email}'
                        LIMIT 1`;

            const dbVendor = (await mysql().query(sql))[0];

            if (! dbVendor) {
                // email not found in database
                return {
                    'success': false,
                    'message': 'This email is not valid.'
                    };
            } else {
                // email exist in dataBase -> check the password
                const resultCompare = bcrypt.compareSync(password, dbVendor.password)

                if (resultCompare) {
                    // get vendor permissions
                    sql  = `SELECT
                                stadium_id AS stadium_id
                            FROM
                                vendor_permissions
                            WHERE
                                vendor_permissions.vendor_id = '${dbVendor.id}'
                            ORDER BY stadium_id `;

                    const dbVendorPermissions = await mysql().query(sql);
                    let listPermissions = [];
                    let keys = Object.keys(dbVendorPermissions);
                    keys.forEach(function(key){
                        listPermissions.push(parseInt(dbVendorPermissions[key].stadium_id));
                    });

                    // get access_token
                    const accessToken = JWT.sign({
                        iss: "Seat",
                        sub: dbVendor.id,
                        email: dbVendor.email,
                        vendor_stadium_id: listPermissions
                        // exp: new Date().setDate(new Date().getDate() )/1000 + settings.tokenLife
                        }, settings.JWT.vendorAccessTokenSecret );

                    return {
                        'success': true,
                        'message': 'Login success',
                        'accessToken': accessToken,
                        'name': dbVendor.name,
                        'email': dbVendor.email,
                        'vendor_stadium_id': listPermissions
                    };

                } else {
                    
                    return {
                        'success': false,
                        'message': 'Login failed, password is not valid.'
                    };
                }
            }
        }
    }



}

module.exports = VendorAuthService;
