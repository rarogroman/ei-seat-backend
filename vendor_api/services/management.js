const mysql = require('../../app/drivers/mysql');
const JWT = require('jsonwebtoken');
const settings = require('../../settings');


class ManagementService {

    /* Get full info about kiosk by kiosk_id */
    static getInfoAboutKiosk(kioskId) {

        return async () => {

            let sql = `SELECT
                            kiosks.kiosk_id AS id,
                            kiosks.kiosk_name AS name,
                            kiosks.kiosk_enable AS enable
                        FROM 
                            kiosks
                        WHERE 
                            kiosks.kiosk_id = '${kioskId}'
                        LIMIT 1 `;

            const result = (await mysql().query(sql))[0];

            if (result) {
                return {
                    success: true,
                    id: result.id,
                    name: result.name,
                    enabled: result.enable? true:false
                };
            } else {
                return {
                    success: false,
                    message: `Kiosk #${kioskId} not found.`
                };
            }
           

        }
    }

    /* Change status the kiosk to offline/online */
    static changeStatusKiosk(accessToken, kioskId, statusKiosk) {

        return async () => {

            // get vendor_id from accessToken
            const decoded = JWT.decode(accessToken, settings.JWT.vendorAccessTokenSecret);
            const vendorId = decoded.sub;
            
            // get list of permissions for vendor
            let sql  = `SELECT kiosks.kiosk_id
                        FROM (vendor_permissions 
                        INNER JOIN stands ON vendor_permissions.stadium_id = stands.stadium_id) 
                        INNER JOIN kiosks ON stands.stand_id = kiosks.stand_id
                        WHERE vendor_permissions.vendor_id='${vendorId}' `;

            const dbVendorPermissions = await mysql().query(sql);

            let listPermissions = [];
            let keys = Object.keys(dbVendorPermissions);
            keys.forEach(function(key){
                listPermissions.push(parseInt(dbVendorPermissions[key].kiosk_id));
            });

            let boolStatusKiosk = (statusKiosk.toLowerCase() === 'true')? 1:0;

            if (listPermissions.includes(kioskId)) {
                sql  = `UPDATE
                            kiosks
                        SET 
                            kiosks.kiosk_enable = '${boolStatusKiosk}'
                        WHERE 
                            kiosks.kiosk_id = '${kioskId}'`;

                const updateResult = await mysql().query(sql);

                if (updateResult.affectedRows) {
                    return {
                        'success': true,
                        'message': 'Kiosk status has been updated.'
                        };
                } else {
                    return {
                        'success': false,
                        'message': 'Operation failed, sql query error.'
                        };
                }
            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

    /* Change status the category of products */
    static changeStatusCategory(accessToken, kioskId, categoryId, statusCategory) {

        return async () => {

            // get vendor_id from accessToken
            const decoded = JWT.decode(accessToken, settings.JWT.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            // get list of permissions for vendor
            let sql  = `SELECT
                            products.category_id
                        FROM (((vendor_permissions 
                        INNER JOIN stands ON vendor_permissions.stadium_id = stands.stadium_id) 
                        INNER JOIN kiosks ON stands.stand_id = kiosks.stand_id) 
                        INNER JOIN warehouses ON kiosks.kiosk_id = warehouses.kiosk_id) 
                        INNER JOIN products ON warehouses.product_id = products.product_id
                        GROUP BY 
                            vendor_permissions.vendor_id, 
                            kiosks.kiosk_id,
                            products.category_id
                        HAVING 
                            (((vendor_permissions.vendor_id)='${vendorId}') AND
                            (kiosks.kiosk_id='${kioskId}')
                        );`;

            const dbVendorPermissions = await mysql().query(sql);

            let listPermissions = [];
            let keys = Object.keys(dbVendorPermissions);
            keys.forEach(function(key){
                listPermissions.push(parseInt(dbVendorPermissions[key].category_id));
            });

            let boolStatusCategory = (statusCategory.toLowerCase() === 'true')? 1:0;

            if (listPermissions.includes(categoryId)) {
                // get list products, which you need to update in the table `warehouses`
                sql =  `SELECT 
                            warehouses.warehouse_id
                        FROM 
                            (((vendor_permissions 
                        INNER JOIN stands ON vendor_permissions.stadium_id = stands.stadium_id) 
                        INNER JOIN kiosks ON stands.stand_id = kiosks.stand_id) 
                        INNER JOIN warehouses ON kiosks.kiosk_id = warehouses.kiosk_id) 
                        INNER JOIN products ON warehouses.product_id = products.product_id
                        GROUP BY products.category_id, vendor_permissions.vendor_id, kiosks.kiosk_id, warehouses.product_id, warehouses.warehouse_id
                        HAVING (((products.category_id)='${categoryId}') AND ((vendor_permissions.vendor_id)='${vendorId}') AND ((kiosks.kiosk_id)='${kioskId}'));`;
                
                const result = await mysql().query(sql);

                let listWarehouseIdForUpdateStatus = [];
                let keys = Object.keys(result);
                keys.forEach(function(key){
                    listWarehouseIdForUpdateStatus.push(parseInt(result[key].warehouse_id));
                });
                
                sql  = `UPDATE
                            warehouses
                        SET 
                            warehouses.warehouse_enable = '${boolStatusCategory}'
                        WHERE 
                            warehouses.warehouse_id IN (${listWarehouseIdForUpdateStatus})`;

                const updateResult = await mysql().query(sql);

                if (updateResult.affectedRows) {
                    return {
                        'success': true,
                        'message': 'Category status has been updated.'
                    };
                } else {
                    return {
                    'success': false,
                    'message': 'Operation failed, sql query error.'
                    };
                }
            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

    /* Change status the product in table warehouses */
    static changeStatusProduct(accessToken, kioskId, productId, statusProduct) {

        return async () => {

            // get vendor_id from accessToken
            const decoded = JWT.decode(accessToken, settings.JWT.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            // get list of permissions for vendor
            let sql  = `SELECT 
                            warehouses.product_id
                        FROM 
                            ((vendor_permissions 
                        INNER JOIN stands ON vendor_permissions.stadium_id = stands.stadium_id) 
                        INNER JOIN kiosks ON stands.stand_id = kiosks.stand_id) 
                        INNER JOIN warehouses ON kiosks.kiosk_id = warehouses.kiosk_id
                        WHERE 
                            (((kiosks.kiosk_id)='${kioskId}') AND 
                            ((vendor_permissions.vendor_id)='${vendorId}'))`;

            const dbVendorPermissions = await mysql().query(sql);

            let listPermissions = [];
            let keys = Object.keys(dbVendorPermissions);
            keys.forEach(function(key){
            listPermissions.push(parseInt(dbVendorPermissions[key].product_id));
            });

            let boolStatusProduct = (statusProduct.toLowerCase() === 'true')? 1:0;

            if (listPermissions.includes(productId)) {
                sql  = `UPDATE
                            warehouses
                        SET 
                            warehouses.warehouse_enable = '${boolStatusProduct}'
                        WHERE 
                            warehouses.product_id = '${productId}' AND 
                            warehouses.kiosk_id = '${kioskId}'`;

                const updateResult = await mysql().query(sql);

                if (updateResult) {
                    return {
                        'success': true,
                        'message': 'Product status has been updated.'
                    };
                } else {
                    return {
                        'success': false,
                        'message': 'Request failed.'
                    };
                }

            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

    /* Get list permissions for vendor where vendor_id in accessToken */
    static getVendorPermissions(accessToken) {

        return async () => {

            // get vendor_id from accessToken
            const decoded = JWT.decode(accessToken, settings.JWT.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            // get list of permissions for vendor
            let sql  = `SELECT 
                            vendor_permissions.stadium_id
                        FROM 
                            vendor_permissions 
                        WHERE 
                            vendor_permissions.vendor_id ='${vendorId}'`;

            const dbVendorPermissions = await mysql().query(sql);

            let listPermissions = [];
            let keys = Object.keys(dbVendorPermissions);
            keys.forEach(function(key){
                listPermissions.push(parseInt(dbVendorPermissions[key].stadium_id));
            });

            return {
                'success': true,
                'vendor_id': vendorId,
                'list_stadiums': listPermissions
            };
        }
    }

    /* Get list stands for stadium_id */
    static getListStandsByStadiumId(accessToken, stadiumId) {

        return async () => {

            let sql = `SELECT
                            stands.stand_id AS id,
                            stands.stand_name AS name
                        FROM 
                            stands
                        WHERE 
                            stands.stadium_id = '${stadiumId}'`;

            const result = await mysql().query(sql);
            
            return result;
        }
    }

    /* Get list kiosks for stand_id */
    static getListKiosksByStandId(accessToken, standId) {

        return async () => {

            let sql = `SELECT
                            kiosks.kiosk_id AS id,
                            kiosks.kiosk_name AS name,
                            kiosks.kiosk_enable AS enable
                        FROM 
                            kiosks
                        WHERE 
                            kiosks.stand_id = '${standId}'`;

            const result = await mysql().query(sql);

            for (const item in result) {
                result[item].enable = result[item].enable? true:false;
            }
           
            return result;
        }
    }

    /* Get list products for kiosk_id */
    static getListProductsForKiosk (accessToken, kioskId) {

        return async () => {

            let sql = `SELECT 
                            products.product_id AS id, 
                            products.product_name AS name,
                            warehouses.warehouse_enable AS enabled 
                        FROM warehouses 
                        INNER JOIN 
                            products ON warehouses.product_id = products.product_id
                        WHERE warehouses.kiosk_id='${kioskId}';`;

            const result = await mysql().query(sql);

            for (const item in result) {
                result[item].enabled = result[item].enabled? true:false;
            }

            return result;

        }
    }

    
}

module.exports = ManagementService;