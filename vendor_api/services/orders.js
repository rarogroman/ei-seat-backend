const mysql = require('../../app/drivers/mysql');
const VendorDbUtils = require('../../app/helpers/vendor_db_utils');
const settings = require('../../settings');


class OrdersService {

    /* Get list orders for kiosk_id for TODAY with status PENDING */
    static getPendingOrders(vendorId, kioskId) {

        return async () => {

            const listVendorPermissions = await VendorDbUtils.getVendorPermissionsKiosks(vendorId)();
            // Check that vendor have permissions for this operation
            if (listVendorPermissions.includes(kioskId)) {

                const sql  =   `SELECT
                                    orders.order_id AS id,
                                    users.user_name AS client_name,
                                    users.user_phone_number AS client_phone,
                                    orders.order_delivery AS delivery, 
                                    orders.order_delivery_section, 
                                    orders.order_delivery_row, 
                                    orders.order_delivery_seat, 
                                    orders.order_date_created AS created, 
                                    orders.order_readiness_time, 
                                    orders.order_status, 
                                    Sum(order_item_quantity*order_item_price ) AS total_price
                                FROM 
                                    orders 

                                INNER JOIN order_items ON orders.order_id = order_items.order_id
                                INNER JOIN users ON orders.user_id = users.user_id

                                GROUP BY 
                                    orders.order_id, 
                                    orders.order_kiosk_id, 
                                    orders.order_delivery, 
                                    orders.order_delivery_section, 
                                    orders.order_delivery_row, 
                                    orders.order_delivery_seat, 
                                    orders.order_date_created, 
                                    orders.order_readiness_time, 
                                    orders.order_status,
                                    orders.user_id
                                HAVING ( 
                                    ( orders.order_kiosk_id = ${kioskId} ) AND
                                    ( orders.order_status ='PENDING' ) AND
                                    ( DATE(orders.order_date_created) = CURDATE() )             
                                    );`;

                const listOrders = await mysql().query(sql);

                for (const item in listOrders) {
                    listOrders[item].delivery = listOrders[item].delivery? true:false;
                }
                
                if (!listOrders) {
                    return {
                        'success': false,
                        'message': 'Request mysql failed.'
                    };
                }
                return listOrders;

            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

    /* Get list orders for kiosk_id for TODAY with status IN ('PREPARING', 'READY_FOR_COLLECTION', 'ON_ITS_WAY') */
    static getActiveOrders(vendorId, kioskId) {

        return async () => {

            const listVendorPermissions = await VendorDbUtils.getVendorPermissionsKiosks(vendorId)();

            // Check that vendor have permissions for this operation
            if (listVendorPermissions.includes(kioskId)) {

                const sql  =   `SELECT
                                    orders.order_id AS id,
                                    users.user_name AS client_name,
                                    users.user_phone_number AS client_phone,
                                    orders.order_delivery AS delivery, 
                                    orders.order_delivery_section, 
                                    orders.order_delivery_row, 
                                    orders.order_delivery_seat, 
                                    orders.order_date_created AS created, 
                                    orders.order_readiness_time, 
                                    orders.order_status, 
                                    Sum(order_item_quantity*order_item_price ) AS total_price
                                FROM 
                                    orders 

                                INNER JOIN order_items ON orders.order_id = order_items.order_id
                                INNER JOIN users ON orders.user_id = users.user_id
                                GROUP BY 
                                    orders.order_id, 
                                    orders.order_kiosk_id, 
                                    orders.order_delivery, 
                                    orders.order_delivery_section, 
                                    orders.order_delivery_row, 
                                    orders.order_delivery_seat, 
                                    orders.order_date_created, 
                                    orders.order_readiness_time, 
                                    orders.order_status,
                                    orders.user_id
                                HAVING ( 
                                    ( orders.order_kiosk_id = ${kioskId} ) AND
                                    ( orders.order_status IN ('PREPARING', 'READY_FOR_COLLECTION', 'ON_ITS_WAY') )  AND
                                    ( DATE(orders.order_date_created) = CURDATE() )               
                                    );`;

                const listOrders = await mysql().query(sql);

                for (const item in listOrders) {
                    listOrders[item].delivery = listOrders[item].delivery? true:false;
                }
                
                if (!listOrders) {
                    return {
                        'success': false,
                        'message': 'Request mysql failed.'
                    };
                }
                return listOrders;

            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

    /* Get the SUM and COUNT for orders with status IN ('COLLECTED', 'DELIVERED') for TODAY */
    static getTodayOrders(vendorId, kioskId) {

        return async () => {

            const listVendorPermissions = await VendorDbUtils.getVendorPermissionsKiosks(vendorId)();

            // Check that vendor have permissions for this operation
            if (listVendorPermissions.includes(kioskId)) {

                const sql  =   `SELECT
                                    orders.order_id AS id,
                                    users.user_name AS client_name,
                                    users.user_phone_number AS client_phone,
                                    orders.order_delivery AS delivery, 
                                    orders.order_delivery_section, 
                                    orders.order_delivery_row, 
                                    orders.order_delivery_seat, 
                                    orders.order_date_created AS created, 
                                    orders.order_readiness_time, 
                                    orders.order_status, 
                                    Sum(order_item_quantity*order_item_price ) AS total_price
                                FROM 
                                    orders 

                                INNER JOIN order_items ON orders.order_id = order_items.order_id
                                INNER JOIN users ON orders.user_id = users.user_id
                                
                                GROUP BY 
                                    orders.order_id, 
                                    orders.order_kiosk_id, 
                                    orders.order_delivery, 
                                    orders.order_delivery_section, 
                                    orders.order_delivery_row, 
                                    orders.order_delivery_seat, 
                                    orders.order_date_created, 
                                    orders.order_readiness_time, 
                                    orders.order_status,
                                    orders.user_id
                                HAVING ( 
                                    ( orders.order_kiosk_id = ${kioskId} ) AND
                                    ( orders.order_status IN ('COLLECTED', 'DELIVERED', 'DECLINED') )  AND
                                    ( DATE(orders.order_date_created) = CURDATE() )               
                                    );`;

                const listOrders = await mysql().query(sql);

                for (const item in listOrders) {
                    listOrders[item].delivery = listOrders[item].delivery? true:false;
                }
                
                if (!listOrders) {
                    return {
                        'success': false,
                        'message': 'Request mysql failed.'
                    };
                }
                return listOrders;

            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

    /* Get the SUM and COUNT for orders with status IN ('COLLECTED', 'DELIVERED') for TODAY */
    static getSummaryOrders(vendorId, kioskId) {

        return async () => {

            const listVendorPermissions = await VendorDbUtils.getVendorPermissionsKiosks(vendorId)();

            // Check that vendor have permissions for this operation
            if (listVendorPermissions.includes(kioskId)) {

                let sql  = `SELECT 
                                Sum(order_item_price*order_items.order_item_quantity) AS total
                            FROM 
                                orders 
                            INNER JOIN 
                                order_items ON orders.order_id = order_items.order_id
                            GROUP BY 
                                orders.order_status, orders.order_id, orders.order_kiosk_id, orders.order_date_created
                            HAVING 
                                orders.order_kiosk_id = '${kioskId}' AND
                                ( DATE(orders.order_date_created) = CURDATE() ) AND
                                orders.order_status = "DELIVERED";`;

                const summaryOrdersDelivered = await mysql().query(sql);

                sql  = `SELECT 
                            Sum(order_item_price*order_items.order_item_quantity) AS total
                        FROM 
                            orders 
                        INNER JOIN 
                            order_items ON orders.order_id = order_items.order_id
                        GROUP BY 
                            orders.order_status, orders.order_id, orders.order_kiosk_id, orders.order_date_created
                        HAVING 
                            orders.order_kiosk_id = '${kioskId}' AND
                            ( DATE(orders.order_date_created) = CURDATE() ) AND 
                            orders.order_status = "COLLECTED";`;

                const summaryOrdersCollecteed = await mysql().query(sql);

                // get sum for COLLECTED and DELIVERED orders
                let deliveredTotal = 0,
                    collectedTotal = 0;

                for (const item of Object.keys(summaryOrdersDelivered)) {
                    deliveredTotal += summaryOrdersDelivered[item].total;
                }

                for (const item of Object.keys(summaryOrdersCollecteed)) {
                    collectedTotal += summaryOrdersCollecteed[item].total;
                }

                return {
                    'TOTAL': {
                        'count': summaryOrdersDelivered.length + summaryOrdersCollecteed.length,
                        'total': +(deliveredTotal + collectedTotal).toFixed(2)
                    },
                    'DELIVERED': {
                        'count': summaryOrdersDelivered.length,
                        'total': deliveredTotal.toFixed(2)
                    },
                    'COLLECTED': {
                        'count': summaryOrdersCollecteed.length,
                        'total': collectedTotal.toFixed(2)
                    }
                }

            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    
    }

    /* Change status for order_id */
    static changeStatusOrder(vendorId, orderId, newStatus, readinessTime = NaN) {

        return async () => {

            // Check that the new order status in the list of allowed
            if ( !(settings.ORDER_STATUSES.includes(newStatus))) {
                return {
                    'success': false,
                    'message': `Operation failed, status must have one of the following values: ${settings.ORDER_STATUSES}.`
                    };
            }

            const listOrdersForVendor = await VendorDbUtils.getVendorOrders(vendorId)();
            
            // Check that vendor have permissions for this operation
            if (listOrdersForVendor.includes(orderId)) {
                
                let sql;

                if (newStatus == "PREPARING") {   // if new status = "PREPARING" then update time created the order

                    sql  = `UPDATE 
                                orders
                            SET 
                                orders.order_date_created = NOW(),
                                orders.order_status = '${newStatus}',
                                orders.order_readiness_time = '${readinessTime}'
                            WHERE
                                orders.order_id = '${orderId}'`;

                } else if (Number.isNaN(readinessTime)) {

                    sql  = `UPDATE 
                                orders
                            SET 
                                orders.order_status = '${newStatus}'
                            WHERE
                                orders.order_id = '${orderId}'`;
                } else {
                    sql  = `UPDATE 
                                orders  
                            SET 
                                orders.order_status = '${newStatus}',
                                orders.order_readiness_time = '${readinessTime}'
                            WHERE
                                orders.order_id = '${orderId}'`;
                }

                let updateResult = await mysql().query(sql);

                if (updateResult.affectedRows) {

                    // insert new status into table `order_statuses`
                    sql = `INSERT INTO order_notifications (
                                order_notifications.order_id,
                                order_notifications.order_notification_status,
                                order_notifications.order_notification_date )
                            VALUES (
                                '${orderId}',
                                '${newStatus}',
                                NOW() )`;

                    updateResult = await mysql().query(sql);
            
                    if (! updateResult.affectedRows) {
                        console.log('Operation failed, sql query error');
                        return {
                            'success': false,
                            'message': 'Operation failed, sql query error.'
                            };
                    }

                    // get current info about order
                    sql  = `SELECT * FROM orders WHERE orders.order_id = '${orderId}' LIMIT 1`;
                    const order = (await mysql().query(sql))[0];

                    return {
                        'success': true,
                        'message': 'The status of the order was updated.',
                        'order': order
                    };
                } else {
                    return {
                    'success': false,
                    'message': 'Operation failed, sql query error.'
                    };
                }

            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

    /* Change status list orders */
    static changeStatusListOrders(vendorId, listOrders, newStatus, readinessTime = NaN) {

        return async () => {

            // Check that the new order status in the list of allowed
            if ( !(settings.ORDER_STATUSES.includes(newStatus))) {
                return {
                    'success': false,
                    'message': `Operation failed, status must have one of the following values: ${settings.ORDER_STATUSES}.`
                    };
            }

            const listOrdersForVendor = await VendorDbUtils.getVendorOrders(vendorId)();

            // Check that vendor have permissions for this operation
            if (listOrders.every(r=> listOrdersForVendor.indexOf(r) >= 0)) {
                let sql;
                if (newStatus == "PREPARING") {   // if new status = "PREPARING" then update time created the order

                    sql  = `UPDATE 
                                orders
                            SET 
                                orders.order_date_created = NOW(),
                                orders.order_status = '${newStatus}',
                                orders.order_readiness_time = '${readinessTime}'
                            WHERE
                                orders.order_id IN (${listOrders})`;
                } else if (Number.isNaN(readinessTime)) {
                    sql  = `UPDATE 
                                orders
                            SET 
                                orders.order_status = '${newStatus}'
                            WHERE
                                orders.order_id IN (${listOrders})`;
                } else {
                    sql  = `UPDATE 
                                orders  
                            SET 
                                orders.order_status = '${newStatus}',
                                orders.order_readiness_time = '${readinessTime}'
                            WHERE
                                orders.order_id IN (${listOrders})`;
                }

                let updateResult = await mysql().query(sql);

                if (updateResult.affectedRows = listOrders.length) {

                    // Create sql for insert miltiple rows in table notifications
                    let sqlInsertNotifications = 'INSERT INTO order_notifications (order_notifications.order_id, order_notifications.order_notification_status, order_notifications.order_notification_date) VALUES';
                    for (const item of listOrders) {
                        sqlInsertNotifications += ` ('${item}', '${newStatus}', NOW()),`;
                    }

                    updateResult = await mysql().query(sqlInsertNotifications.slice(0,-1));
            
                    if (! (updateResult.affectedRows = listOrders.length)) {
                        return {
                            'success': false,
                            'message': 'Operation failed, sql query error.'
                            };
                    }

                    // get current info about order
                    sql  = `SELECT * FROM orders WHERE orders.order_id IN (${listOrders})`;
                    const orders = await mysql().query(sql);
console.log('--- orders: ----');
console.log(orders);
console.log('-------------');
                    return {
                        'success': true,
                        'message': 'The statuses of all orders was updated.',
                        'orders': orders
                    };

                } else {
                    return {
                    'success': false,
                    'message': 'Operation failed, sql query error.'
                    };
                }

            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

    /* Change readiness time for order_id */
    static changeRaadinessTimeOrder(vendorId, orderId, readinessTime) {

        return async () => {

            const listOrdersForVendor = await VendorDbUtils.getVendorOrders(vendorId)();

            // Check that vendor have permissions for this operation
            if (listOrdersForVendor.includes(orderId)) {
                let sql  = `UPDATE 
                                orders
                            SET 
                                orders.order_readiness_time = '${readinessTime}'
                            WHERE
                                orders.order_id = '${orderId}'`;

                const updateResult = await mysql().query(sql);

                if (updateResult.affectedRows) {
                    return {
                        'success': true,
                        'message': 'The readiness time for order has been updated.'
                    };
                } else {
                    return {
                    'success': false,
                    'message': 'Operation failed, sql query error.'
                    };
                }

            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

     /* Get full info about order and customer by order_id */
    static getOrderDetailesByOrderId(vendorId, orderId) {

        return async () => {

            const listOrdersForVendor = await VendorDbUtils.getVendorOrders(vendorId)();

            // Check that vendor have permissions for this operation
            if (listOrdersForVendor.includes(orderId)) {

                // get order detailed info by order_id
                let sql  = `SELECT
                                orders.user_id,
                                orders.order_id,
                                (SELECT stadium_name FROM stadiums WHERE stadium_id = orders.order_stadium_id LIMIT 1) AS stadium,
                                (SELECT stand_name FROM stands WHERE stand_id = orders.order_stand_id LIMIT 1) AS stand,
                                orders.order_stand_id,
                                orders.order_kiosk_id,
                                orders.order_delivery,
                                orders.order_delivery_section,
                                orders.order_delivery_row,
                                orders.order_delivery_seat,
                                orders.order_date_created,
                                orders.order_readiness_time,
                                orders.order_status,
                                (SELECT SUM(t) from (SELECT order_items.order_item_quantity*order_items.order_item_price AS t FROM order_items WHERE order_items.order_id = '${orderId}') as total) as total_price
                            FROM
                                orders 
                            WHERE
                                orders.order_id = '${orderId}'
                            LIMIT 1`;

                const order = (await mysql().query(sql))[0];

                if (! order) {
                    return {
                        'success': false,
                        'message': `Order with number '${orderId}' does not exist.`
                        };
                }

                // get info about customer
                let sqlCustomer = `SELECT
                                        users.user_name AS name,
                                        users.user_email AS email,
                                        users.user_date_of_birth AS date_of_birth,
                                        users.user_phone_number AS phone_number
                                    FROM
                                        users 
                                    WHERE
                                        users.user_id = '${order.user_id}'`;

                const customer = (await mysql().query(sqlCustomer))[0];

                // get order_items for order_id
                let sqlOrderItems = `SELECT
                                        (SELECT product_name FROM products WHERE product_id = order_items.product_id LIMIT 1) AS name,
                                        order_items.order_item_quantity AS quantity,
                                        order_items.order_item_price AS price
                                    FROM
                                        order_items 
                                    WHERE
                                        order_items.order_id = '${orderId}'
                                    ORDER BY order_items.order_item_id`;

                const orderItems = await mysql().query(sqlOrderItems);

                return {
                    'success': true,
                    'order_id': order.order_id,
                    'stadium': order.stadium,
                    'stand': order.stand,
                    'kiosk_id': order.order_kiosk_id,
                    'delivery': order.order_delivery ? true : false,
                    'delivery_section': order.order_delivery_section,
                    'delivery_row': order.order_delivery_row,
                    'delivery_seat': order.order_delivery_seat,
                    'created': (new Date(order.order_date_created-(new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, 19),
                    'readiness_time': order.order_readiness_time,
                    'status': order.order_status,
                    'total_price': order.total_price,
                    'customer': customer,
                    'products': orderItems
                    };

            } else {
                return {
                    'success': false,
                    'message': 'You do not have permission to perform this operation.'
                };
            }
        }
    }

    
}

module.exports = OrdersService;
