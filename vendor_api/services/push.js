const settings = require('../../settings');
const AWS = require('aws-sdk');
const Firebase = require("firebase-admin");
const serviceAccount = require("../../seat-3a384-firebase-adminsdk-tx9ur-66233145fb.json");
const NotificationsService = require('../../client_api/services/notifications');


// initialization the amazon service (push notification to ios devices)
AWS.config.update({
    accessKeyId: 'AKIAIHY5JQNI7HGKXLUA',
    secretAccessKey: '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',
    region: 'us-west-1'
});

// initialization the firebase admin sdk (push notification to android devices)
Firebase.initializeApp({
    credential: Firebase.credential.cert(serviceAccount),
    databaseURL: "https://seat-3a384.firebaseio.com"
});
  

class PushService {

    /* Send push notification to ios or android mobile device */
    static sendPushNotification(deviceOS, deviceToken, title, message, badge, url) {

        return async () => {

            if ((deviceOS == "ios") && deviceToken) {
                this.sendPushToIos(deviceToken, title, message, badge, url)();
            } else if ((deviceOS == "android") && deviceToken) {
                this.sendPushToAndroid(deviceToken, title, message, badge, url)();
            }
        }
    }

    /* Send push notification to ios device */
    static sendPushToIos(deviceToken, title, message, badge, url) {

        return async () => {

            let sns = new AWS.SNS();

            sns.createPlatformEndpoint({
                PlatformApplicationArn: 'arn:aws:sns:us-west-1:914325480188:app/APNS_SANDBOX/seat',
                Token: deviceToken
            }, function(err, data) {
                if (err) {
                    console.log(err.stack);
                    return {
                        'success': false,
                        'message': err.stack
                    };
                }

                let endpointArn = data.EndpointArn;

                let payload = {
                    default: message,
                    APNS_SANDBOX: JSON.stringify({
                        "aps" : {
                            "alert":{
                                "title":title,
                                "body":message
                             },
                            "badge" : badge,
                            "sound" : "default"
                        },
                        "url": url
                    })
                };

                // first have to stringify the inner APNS object...
                payload.APNS = JSON.stringify(payload);
                // then have to stringify the entire message payload
                payload = JSON.stringify(payload);

                sns.publish({
                    Message: payload,
                    MessageStructure: 'json',
                    TargetArn: endpointArn
                }, function(err, data) {
                    if (err) {
                        return {
                            'success': false,
                            'message': err.stack
                        };
                    }

                });
            });

            return {
                'success': true,
                'message': 'Push notification was send successfully.'
            };
        }

    }

    /* Send push notification to android device */
    static sendPushToAndroid(deviceToken, title, message, badge, url) {

        return async () => {
            const payload = {
                notification: {
                    title: title,
                    body: message,
                    sound: "default",
                    badge: badge.toString()
                },
                data: {
                    priority: "high",
                    url: url,
                }
            };

            Firebase.messaging().sendToDevice(deviceToken, payload)
            .catch(function(error) {
                console.log("Error sending push to android device:", error);
            });

            return {
                'success': true,
                'message': 'Push notification was send successfully.'
            };
        }

    }

    /* Send multiple push notifications to list of devices */
    static sendMultiplePushNotifications(orders, newStatus) {

        return async () => {

            const title = "Food Delivery Company";

            for(const item of orders) {
                const deviceOS = item.order_device_os;
                const deviceToken = item.order_device_token;

                const message = settings.ORDER_PUSH_NOTIFICATIONS[newStatus]
                                .replace('ORDNUMBER', item.order_id)
                                .replace('READINESSTIME', item.order_readiness_time)
                                .replace('KIOSKNUMBER', item.order_kiosk_id)
                                .replace('SECTION', item.order_delivery_section)
                                .replace('ROWNUMBER', item.order_delivery_row)
                                .replace('SEATNUMBER', item.order_delivery_seat);

                // get unread count for user_id
                const notificationsInfo = await NotificationsService.getUnreadNotificationsCount(item.user_id)();
                const badge = notificationsInfo.unread_count;
                const url = `seat://status_update?id=${item.order_id}`;

                this.sendPushNotification(deviceOS, deviceToken, title, message, badge, url)();
            }
        }
    }



}

module.exports = PushService;
