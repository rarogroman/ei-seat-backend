const vendorManagementService = require('../services/management');


class VendorManagementController {

    /* Get full info about kiosk by kiosk_id */
    static getInfoAboutKiosk() {

        return async (req, res, next) => {

            const kioskId = parseInt(req.body.kiosk_id);

            if (! kioskId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field kiosk_id is empty.'
                })
            };

            const result = await vendorManagementService.getInfoAboutKiosk(kioskId)();

            if (result.success == true) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Change status the kiosk to offline/online */
    static changeStatusKiosk() {

        return async (req, res, next) => {

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;

            const kioskId = parseInt(req.body.kiosk_id);
            if (! kioskId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field kiosk_id is empty.'
                })
            };

            const statusKiosk = req.body.status_kiosk;
            if (! statusKiosk) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field status_kiosk is empty.'
                })
            };

            const result = await vendorManagementService.changeStatusKiosk(accessToken, kioskId, statusKiosk)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
               if ( (result.message) && (result.message == 'You do not have permission to perform this operation.') ) {
                   return res.status(403).json(result);
               } else {
                return res.status(404).json(result);
               }
            }
        }
    }

    /* Change status the category of products */
    static changeStatusCategory() {

        return async (req, res, next) => {

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;

            const kioskId = parseInt(req.body.kiosk_id);
            if (! kioskId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Fiels kiosk_id is empty.'
                })
            };

            const categoryId = parseInt(req.body.category_id);
            if (! categoryId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field category_id is empty.'
                })
            };

            const statusCategory = req.body.status_category;
            if (! statusCategory) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field status_category is empty.'
                })
            };
            
            const result = await vendorManagementService.changeStatusCategory(accessToken, kioskId, categoryId, statusCategory)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Change status the product in table warehouses */
    static changeStatusProduct() {

        return async (req, res, next) => {

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            
            const kioskId = parseInt(req.body.kiosk_id);

            if (! kioskId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field kiosk_id is empty.'
                })
            };

            const productId = parseInt(req.body.product_id);

            if (! productId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field product_id is empty.'
                })
            };

            const statusProduct = req.body.status_product;

            if (! statusProduct) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field status_product is empty.'
                })
            };
            
            const result = await vendorManagementService.changeStatusProduct(accessToken, kioskId, productId, statusProduct)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Get list permissions for vendor where vendor_id in accessToken */
    static getVendorPermissions() {

        return async (req, res, next) => {

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            
            const result = await vendorManagementService.getVendorPermissions( accessToken )();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Get list stands for stadium_id */
    static getListStandsByStadiumId() {

        return async (req, res, next) => {

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            const stadiumId = parseInt(req.body.stadium_id);
            if (! stadiumId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field stadium_id is empty.'
                })
            };

            const result = await vendorManagementService.getListStandsByStadiumId(accessToken, stadiumId)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Get list kiosks for stand_id */
    static getListKiosksByStandId() {

        return async (req, res, next) => {

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            const standId = parseInt(req.body.stand_id);
            if (! standId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field stand_id is empty.'
                })
            };

            const result = await vendorManagementService.getListKiosksByStandId( accessToken, standId )();

            return res.status(200).json(result);
        }
    }

    /* Get list products for kiosk_id */
    static getListProductsForKiosk() {

        return async (req, res, next) => {

            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            const kioskId = parseInt(req.body.kiosk_id);

            if (! kioskId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field kiosk_id is empty.'
                })
            };

            const result = await vendorManagementService.getListProductsForKiosk(accessToken, kioskId)();

            return res.status(200).json(result);
        }
    }

}

module.exports = VendorManagementController;
