const settings = require('../../settings');
const JWT = require('jsonwebtoken');
const VendorOrdersService = require('../services/orders');
const VendorPushService = require('../services/push');
const NotificationsService = require('../../client_api/services/notifications');
const MailUtils = require('../../app/helpers/mailUtils');


class VendorOrdersController {

    /* Get list orders for kiosk_id for TODAY with status PENDING */
    static getPendingOrders() {

        return async (req, res, next) => {

            // get vendor_id from accessToken
            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            const decoded = JWT.decode(accessToken, settings.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            const kioskId = parseInt(req.body.kiosk_id);
            if (! kioskId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field kiosk_id is empty.'
                })
            };

            const listPendingOrders = await VendorOrdersService.getPendingOrders(vendorId, kioskId)();


            if (listPendingOrders.success !== false) {
                return res.status(200).json(listPendingOrders);
            } else {
               if ( (listPendingOrders.message) && (listPendingOrders.message == 'You do not have permission to perform this operation.') ) {
                   return res.status(403).json(listPendingOrders);
               } else {
                return res.status(404).json(listPendingOrders);
               }
            }


        }
    }

    /* Get list orders for kiosk_id for TODAY with status IN ('PREPARING', 'READY_FOR_COLLECTION', 'ON_ITS_WAY') */
    static getActiveOrders() {

        return async (req, res, next) => {
            // get vendor_id from accessToken
            const accessToken = req.body.accessToken || req.query.accessToken || req.headers.accesstoken;
            const decoded = JWT.decode(accessToken, settings.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            const kioskId = parseInt(req.body.kiosk_id);
            if (! kioskId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field kiosk_id is empty.'
                })
            };

            const listActiveOrders = await VendorOrdersService.getActiveOrders(vendorId, kioskId)();

            if (listActiveOrders.success !== false) {
                return res.status(200).json(listActiveOrders);
            } else {
                return res.status(404).json(listActiveOrders);
            }
        }
    }

    /* Get the list orders with status IN ('COLLECTED', 'DELIVERED') for TODAY */
    static getTodayOrders() {

        return async (req, res, next) => {
            // get vendor_id from accessToken
            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            const decoded = JWT.decode(accessToken, settings.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            const kioskId = parseInt(req.body.kiosk_id);
            if (! kioskId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field kiosk_id is empty.'
                })
            };

            const listTodayOrders = await VendorOrdersService.getTodayOrders(vendorId, kioskId)();

            if (listTodayOrders.success !== false) {
                return res.status(200).json(listTodayOrders);
            } else {
                return res.status(404).json(listTodayOrders);
            }
        }
    }

    /* Get the SUM and COUNT for orders with status IN ('COLLECTED', 'DELIVERED') for TODAY */
    static getSummaryOrders() {

        return async (req, res, next) => {
            // get vendor_id from accessToken
            const accessToken = req.body.accessToken || req.query.accessToken || req.headers.accesstoken;
            const decoded = JWT.decode(accessToken, settings.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            const kioskId = parseInt(req.body.kiosk_id);
            if (! kioskId ) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field kiosk_id is empty.'
                })
            };

            const summaryOrders = await VendorOrdersService.getSummaryOrders(vendorId, kioskId)();

            if (summaryOrders.success !== false) {
                return res.status(200).json(summaryOrders);
            } else {
                return res.status(404).json(summaryOrders);
            }
        }
    }

    /* Change status for order_id */
    static changeStatusOrder() {

        return async (req, res, next) => {
            // get vendor_id from accessToken
            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            const decoded = JWT.decode(accessToken, settings.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            const orderId = parseInt(req.body.order_id);
            if (! orderId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field order_id is empty.'
                })
            };

            const newStatus = req.body.status;
            if (! newStatus) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field status is empty.'
                })
            };

            const readinessTime = parseInt(req.body.readiness_time);
            if (req.body.readiness_time && !(Number.isInteger(readinessTime))) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field readiness_time is not number.'
                })
            };

            if (req.body.readiness_time && readinessTime<0) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field readiness_time is less than 0.'
                })
            };
            // Change status of order and add record in table notifications
            const result = await VendorOrdersService.changeStatusOrder(vendorId, orderId, newStatus, readinessTime)();

            if (result.success &&
                    Object.keys(settings.ORDER_PUSH_NOTIFICATIONS).includes(newStatus) &&
                    result.order.order_device_token && (newStatus != 'PREPARING') && (newStatus != 'DECLINED')) {

                /* ============ SEND PUSH NOTIFICATION ============ */
                const deviceOS = result.order.order_device_os;
                const deviceToken = result.order.order_device_token;

                const message = settings.ORDER_PUSH_NOTIFICATIONS[newStatus]
                                .replace('ORDNUMBER', orderId)
                                .replace('READINESSTIME', readinessTime)
                                .replace('KIOSKNUMBER', result.order.order_kiosk_id)
                                .replace('SECTION', result.order.order_delivery_section)
                                .replace('ROWNUMBER', result.order.order_delivery_row)
                                .replace('SEATNUMBER', result.order.order_delivery_seat);

                const title = "Food Delivery Company";

                // get unread count for user_id
                const notificationsInfo = await NotificationsService.getUnreadNotificationsCount(result.order.user_id)();
                const badge = notificationsInfo.unread_count;
                const url = `seat://status_update?id=${result.order.order_id}`;

                //send push notification
                VendorPushService.sendPushNotification(deviceOS, deviceToken, title, message, badge, url)();
            }

            //  =============== Send email to client that the order was created if status = 'PREPARING'
            if (newStatus == 'PREPARING') {
//                console.log('-------------------------send mail--------------------------------');

//                const order = await VendorOrdersService.getOrderDetailesByOrderId(vendorId, result.order.order_id)();
                //MailUtils.sendMailAboutCreatedOrder(order)();
            }

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Change status list orders */
    static changeStatusListOrders() {

        return async (req, res, next) => {
            // get vendor_id from accessToken
            const accessToken = req.headers.accesstoken || req.body.accessToken || req.query.accessToken;
            const decoded = JWT.decode(accessToken, settings.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

console.log('---------');
console.log(req.body);
console.log('---------');
            const listOrders = req.body.orders;
            if (! listOrders) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field orders is empty.'
                })
            };

            const newStatus = req.body.status;
            if (! newStatus) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field status is empty.'
                })
            };

            const readinessTime = parseInt(req.body.readiness_time);
            if (req.body.readiness_time && !(Number.isInteger(readinessTime))) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field readiness_time is not number.'
                })
            };

            if (req.body.readiness_time && readinessTime<0) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field readiness_time is less than 0.'
                })
            };

            const result = await VendorOrdersService.changeStatusListOrders(vendorId, listOrders, newStatus, readinessTime)();
//console.log('result: ', result);
            /* ============ SEND MULTIPLE PUSH NOTIFICATIONS ============ */
            if ((result.success !== false) && Object.keys(settings.ORDER_PUSH_NOTIFICATIONS).includes(newStatus) && (newStatus != 'PREPARING') && (newStatus != 'DECLINED')) {
                VendorPushService.sendMultiplePushNotifications(result.orders, newStatus)();
            }

            if (result.success !== false) {
                return res.status(200).json({
                    'success': true,
                    'message': 'The statuses of all orders have been updated.',
                   'orders': [
 
{ 
    "order_id": 906,
    "stadium": "Melbourne Cricket Ground",
    "stand": "My",
    "kiosk_id": 33,
    "delivery": false,
    "delivery_section": null,
    "delivery_row": null,
    "delivery_seat": null,
    "created": "2018-08-31T11:25:44",
    "readiness_time": 20, 
    "status": "PREPARING", 
 
   "total_price": 83.5,
    "customer": {
        "name": "qqq@hotmail.com",
        "email": "qqq@hotmail.com",
        "date_of_birth": null,
        "phone_number": null
    },
    "products": [
        {
            "name": "Olympic Onion Rings",
            "quantity": 5,
            "price": 16.7
        }
     ]
},


{
    "order_id": 907,
    "stadium": "Melbourne Cricket Ground",
    "stand": "My",
    "kiosk_id": 33,
    "delivery": false,
    "delivery_section": null,
    "delivery_row": null,
    "delivery_seat": null,
    "created": "2018-08-31T11:25:44",
    "readiness_time": 20,
    "status": "PREPARING",
    "total_price": 83.5,
    "customer": {
        "name": "qqq@hotmail.com",
        "email": "qqq@hotmail.com",
        "date_of_birth": null,
        "phone_number": null
    },
    "products": [
        {
            "name": "Olympic Onion Rings",
            "quantity": 5,
            "price": 16.7
        }
     ]
},


{
    "order_id": 908,
    "stadium": "Melbourne Cricket Ground",
    "stand": "My",
    "kiosk_id": 33,
    "delivery": false,
    "delivery_section": null,
    "delivery_row": null,
    "delivery_seat": null,
    "created": "2018-08-31T11:25:44",
    "readiness_time": 20,
    "status": "PREPARING",
    "total_price": 83.5,
    "customer": {
        "name": "qqq@hotmail.com",
        "email": "qqq@hotmail.com",
        "date_of_birth": null,
        "phone_number": null
    },
    "products": [
        {
            "name": "Olympic Onion Rings",
            "quantity": 5,
            "price": 16.7
        }
     ]
}

]



});
















            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Change readiness time for order_id */
    static changeRaadinessTimeOrder() {

        return async (req, res, next) => {
            
            // get vendor_id from accessToken
            const accessToken = req.body.accessToken || req.query.accessToken || req.headers.accesstoken;
            const decoded = JWT.decode(accessToken, settings.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            const orderId = parseInt(req.body.order_id);

            if (! orderId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field order_id is empty.'
                })
            };

            const readinessTime = parseInt(req.body.readiness_time);

            if ( !(Number.isInteger(readinessTime)) || readinessTime<0 ) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field readiness_time is empty or less 0.'
                })
            };

            const result = await VendorOrdersService.changeRaadinessTimeOrder(vendorId, orderId, readinessTime)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }

    /* Get full info about order and customer by order_id */
    static getOrderDetailesByOrderId() {

        return async (req, res, next) => {
            
            // get vendor_id from accessToken
            const accessToken = req.body.accessToken || req.query.accessToken || req.headers.accesstoken;
            const decoded = JWT.decode(accessToken, settings.vendorAccessTokenSecret);
            const vendorId = decoded.sub;

            const orderId = parseInt(req.body.order_id);

            if (! orderId) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field order_id is empty.'
                })
            };

            const result = await VendorOrdersService.getOrderDetailesByOrderId(vendorId, orderId)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }
 
}

module.exports = VendorOrdersController;
