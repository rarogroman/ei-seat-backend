const VendorAuthService = require('../services/auth');


class VendorAuthController {

    /* Register new vendor in table vendors with not permissions */
    static vendorRegister() {

        return async (req, res, next) => {

            if (! req.body.email) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field email is empty.'
                });
            }

            if (! req.body.password) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field password is empty.'
                });
            }

            if (! req.body.name) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field name is empty.'
                });
            }

            const resultRegister = await VendorAuthService.vendorRegister(req.body.email, req.body.password, req.body.name)();

            if (resultRegister.success !== false) {
                return res.status(200).json(resultRegister);
            } else {
                return res.status(404).json(resultRegister);
            }
        }
    }

    /* Login vendor, return accessToken */
    static vendorLogin() {

        return async (req, res, next) => {

            if (! req.body.email) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field email is empty.'
                });
            }

            if (! req.body.password) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field password is empty.'
                });
            }

            const resultLogin = await VendorAuthService.vendorLogin(req.body.email, req.body.password)();
console.log(resultLogin);
            if (resultLogin.success !== false) {
                return res.status(200).json(resultLogin);
            } else {
                return res.status(404).json(resultLogin);
            }
        }
    }

}

module.exports = VendorAuthController;
