const VendorPushService = require('../services/push');


class VendorPushController {

    /* Send push notification to mobile device ios or android*/
    static sendPushNotification() {

        return async (req, res, next) => {

            const deviceToken = req.body.device_token;

            let title = "title";
            let badge = 9;
            let url = "";

            if (! deviceToken) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field device_token is empty.'
                })
            };

            const message = req.body.message;
            if (! message) {
                return res.status(404).json({
                    'success': false,
                    'message': 'Field message is empty.'
                })
            };

            const result = await VendorPushService.sendPushToAndroid(deviceToken, title, message, badge, url)();

            if (result.success !== false) {
                return res.status(200).json(result);
            } else {
                return res.status(404).json(result);
            }
        }
    }


 
}

module.exports = VendorPushController;