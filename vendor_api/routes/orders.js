const express = require('express');
const router = express.Router();
const VendorOrdersController = require('../controllers/orders');
const asyncHandler = require('express-async-handler');
const VerifyVendorToken = require('../../app/middlewares/verifyVendorToken');


router.post('/pending', VerifyVendorToken,
    asyncHandler(VendorOrdersController.getPendingOrders())
);

router.post('/active', VerifyVendorToken,
    asyncHandler(VendorOrdersController.getActiveOrders())
);

router.post('/today', VerifyVendorToken,
    asyncHandler(VendorOrdersController.getTodayOrders())
);

router.post('/summary', VerifyVendorToken,
    asyncHandler(VendorOrdersController.getSummaryOrders())
);

router.post('/change-status', VerifyVendorToken,
    asyncHandler(VendorOrdersController.changeStatusOrder())
);

router.post('/change-list-statuses', VerifyVendorToken,
    asyncHandler(VendorOrdersController.changeStatusListOrders())
);

router.post('/change-readiness-time', VerifyVendorToken,
    asyncHandler(VendorOrdersController.changeRaadinessTimeOrder())
);

router.post('/customer-details', VerifyVendorToken,
    asyncHandler(VendorOrdersController.getOrderDetailesByOrderId())
);



module.exports = router;