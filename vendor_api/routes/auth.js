const express = require('express');
const router = express.Router();
const VendorAuthController = require('../controllers/auth');
const asyncHandler = require('express-async-handler');

router.post('/register',
    asyncHandler(VendorAuthController.vendorRegister())
);

router.post('/login',
    asyncHandler(VendorAuthController.vendorLogin())
);

module.exports = router;