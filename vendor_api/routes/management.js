const express = require('express');
const router = express.Router();
const VendorManagementController = require('../controllers/management');
const asyncHandler = require('express-async-handler');
const VerifyVendorToken = require('../../app/middlewares/verifyVendorToken');


router.post('/permissions', VerifyVendorToken,
    asyncHandler(VendorManagementController.getVendorPermissions())
);

router.post('/kiosk', VerifyVendorToken,
    asyncHandler(VendorManagementController.getInfoAboutKiosk())
);

router.post('/kiosk-status', VerifyVendorToken,
    asyncHandler(VendorManagementController.changeStatusKiosk())
);

router.post('/product-status', VerifyVendorToken,
    asyncHandler(VendorManagementController.changeStatusProduct())
);

router.post('/category-status', VerifyVendorToken,
    asyncHandler(VendorManagementController.changeStatusCategory())
);

router.post('/stands', VerifyVendorToken,
    asyncHandler(VendorManagementController.getListStandsByStadiumId())
);

router.post('/kiosks', VerifyVendorToken,
    asyncHandler(VendorManagementController.getListKiosksByStandId())
);

router.post('/kiosk-list-products', VerifyVendorToken,
    asyncHandler(VendorManagementController.getListProductsForKiosk())
);


module.exports = router;