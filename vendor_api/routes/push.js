const express = require('express');
const router = express.Router();
const VendorPushController = require('../controllers/push');
const asyncHandler = require('express-async-handler');


router.post('/send',
    asyncHandler(VendorPushController.sendPushNotification())
);



module.exports = router;